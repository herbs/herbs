.. @+leo-ver=4-encoding=iso-8859-1.
.. @+node:@file dev/doc/CdC.txt
.. @@language rst

Cahier des charges HERBS
========================

Objectifs
---------
 
La r��criture de la plateforme HERBS doit r�pondre � un certain nombre
d'exigences.


Plateformes d'accueil
  Ex�cution possible sur les plateformes un*x (modernes), Mac OS X,
  Windows.  L'accent doit �tre mis en priorit� sur windows + linux, sans
  pour autant perdre de vue les autres plateformes.

Proc�dure d'installation
  L'installation doit �tre facile, i.e. accessible � un
  non-informaticien.  L'id�al est une installation "clique-bouton" d'un
  seul tenant (wizard p.ex., ou prg. d'iunstallation d�di� et automatis�
  au maximum).

Taille des donn�es trait�es
  On peut faire l'hypoth�se, dans un premier temps, que les donn�es
  trait�es et les r�sultats peuvent tenir en m�moire au runtime. On se
  base sur une configuration type Windows XP, 256Mo de m�moire, sans
  autre programme qui tourne. L'architecture devra cependant �tre con�u
  de fa�on � ce que l'on puisse raisonnablement revenir plus tard sur
  cette hypoth�se.

Maintenabilit�
  L'architecture et le code produit doit pouvoir facilement �tre repris en
  main.  Cela implique �galement le choix d'un langage "facile", ainsi qu'une
  documentation suffisante; la documentation devra �galement faire �tat des
  m�thodolgies et des proc�dures d'extension du logiciel (extension en termes
  de: formats de fichiers pour l'import ou l'export de donn�es, ajout de
  traitements et algorithmes suppl�mentaires)

Objectifs fonctionnels
~~~~~~~~~~~~~~~~~~~~~~
La nouvelle plateforme devra remplir toutes les fonctionnalit�s de
simulation actuellement implant�es, en particulier et de fa�on non
exhaustive:

  - traitements mono-indices: analyse de base de r�gles, distributions
    (4 traitements distincts), filtrage,

  - traitements multi-indices: filtrage, comparaisons de pr�ordres
    induits par k indices, comparaisons des distributions pour deux
    indices.

  - possibilit� de produire des graphiques sous diff�rents formats (jpg ou
    gif, et postscript minimalement)

.. note:: ces traitements sont � d�tailler en termes d'I/O + traitements
   en eux-m�mes.

De surcro�t, on assurera �galement:

  - chargement de bases de cas (BdC) (formats: apriori, c4.5), de bases
    de r�gles (BdR) (formats � pr�ciser)

  - interface d'associations BdC / BdR (compatibilit�)

  - il me semble que nous avions parl� avec Beno�t de mesures
    sur les bases de cas, pr�liminaires � la production de r�gle par
    `apriori` --support et confiance si mes souvenirs sont bons. Est-ce
    que cela a du sens et si oui, quels sont ces traitements?

Contraintes inh�rentes aux traitements
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Il conviendra de s'assurer que les traitements se feront en "temps
  raisonnable". Pour cela, il faudra d�finir un ou plusieurs jeux de
  donn�es/traitements auxquels on associera un ordre de grandeur pour le
  temps d'ex�cution.
  

Extensions possibles
~~~~~~~~~~~~~~~~~~~~
  La refonte de l'outil devra se faire avec l'id�e que les
  fonctionnalit�s suivantes seront demand�es lors de d�veloppements
  ult�rieurs:

  - conservation des r�sultats, en particulier pour les traitements qui
    produisent des base de cas ou des base de r�gles

  - possibilit� de cha�ner les op�rations (p.ex. filtrage, puis comparaison)
    de fa�on � pouvoir exprimer des cha�nes de traitements compl�tes (�
    pr�ciser/d�finir)

  - (identifi� comme prioritaire lors de nos discussions avec Beno�t)
    possibilit� d'implanter des m�thodes statistiques type bootsrap ou
    validation crois�e. **Ce point doit �tre pr�cis� plus en d�tail**.

  - possibilit� d'associer aux mesures des propri�t�s formelles ou
    exp�rimentales (exp�rimentales==d�pendantes des donn�es, p.ex. la
    mesure est-elle discriminante?). L'id�e sous-jacente est de pouvoir
    transmettre les-dites mesures et leurs propri�t�s � un logiciel tel
    que Prom�th�e. NB: cela aiderait **beaucoup** d'avoir quelques
    use-cases concrets ici.

Divers
------

.. _`HERBS Home Page`: http://www-iasc.enst-bretagne.fr/ecd-ind/HERBS/utilitaires.php



.. @-node:@file dev/doc/CdC.txt
.. @-leo
