#! /usr/bin/perl

##########################################
# Copyright (C) 2002 Benoit Vaillant     #
# (benoit.vaillant@enst-bretagne.fr)     #
# This file is part of the HERBS project #
#                                        #
# see COPYRIGHT.TXT                      #
##########################################

package chi2;

sub new {
  my $classe = shift;
  my $self = { };
  bless($self, $classe);
  return $self;
}

sub nom {
  return "Chi 2";
}

sub valeur {
  shift;
  my ($idCR, $idRegle, $n, $DB_NAME, $DB_HOST, $DB_PORT, $DB_USER_NAME, $DB_PASSWD) = @_;
  my $sth;
  my $bd = DBI->connect("DBI:mysql:$DB_NAME:$DB_HOST:$DB_PORT", $DB_USER_NAME, $DB_PASSWD, {PrintError => 0, RaiseError => 0 } ) or $erreur = 1;
  if ($erreur) {
    print("--\nImpossible de se connecter � la base de donn�e.\nV�rifiez les param�tres de configuration de HERBS.\n");
    return undef;
  }
  else {
    $sth = $bd->prepare("SELECT na, nb, nab, nab_ from evaluations where idCR=$idCR and idRegle=$idRegle");
    $sth->execute;
    while(@row = $sth->fetchrow_array) { # un seul tuple normalement
      $na = $row[0];
      $nb = $row[1];
      $nab = $row[2];
      $nab_= $row[3];
      if ((!$na) or ($nb == $n) or (!$nb) or ($na == $n)) { return 0; } # ne doit pas arriver normalement
      return $n*($n*$nab - $na*$nb)*($n*$nab - $na*$nb)/($na*$nb*($n-$na)*($n-$nb));
    }
  }
}

sub hi {
  return 0;
}

return 1;
