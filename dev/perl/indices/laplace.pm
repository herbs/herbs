#! /usr/bin/perl

##########################################
# Copyright (C) 2002 Benoit Vaillant     #
# (benoit.vaillant@enst-bretagne.fr)     #
# This file is part of the HERBS project #
#                                        #
# see COPYRIGHT.TXT                      #
##########################################

package laplace;

sub new {
  my $classe = shift;
  my $self = { };
  bless($self, $classe);
  return $self;
}

sub nom {
  return "Laplace";
}

sub valeur {
    shift;
    my ($idCR, $idRegle, $n, $DB_NAME, $DB_HOST, $DB_PORT, $DB_USER_NAME, $DB_PASSWD) = @_;
    my $sth;
    my $bd = DBI->connect("DBI:mysql:$DB_NAME:$DB_HOST:$DB_PORT", $DB_USER_NAME, $DB_PASSWD, {PrintError => 0, RaiseError => 0 } ) or $erreur = 1;
    if ($erreur) {
	print("--\nImpossible de se connecter � la base de donn�e.\nV�rifiez les param�tres de configuration de HERBS.\n");
	return undef;
    }
    else {
	$sth = $bd->prepare("SELECT na, nab from evaluations where idCR=$idCR and idRegle=$idRegle");
	$sth->execute;
	while(@row = $sth->fetchrow_array) { # un seul tuple normalement
	    $na = $row[0];
	    $nab = $row[1];
	    return ($nab+1)/($na+2);
	}
    }
}

sub hi {
    shift;
    my ($idCR, $idRegle, $n, $DB_NAME, $DB_HOST, $DB_PORT, $DB_USER_NAME, $DB_PASSWD) = @_;
    my $sth;
    my $bd = DBI->connect("DBI:mysql:$DB_NAME:$DB_HOST:$DB_PORT", $DB_USER_NAME, $DB_PASSWD, {PrintError => 0, RaiseError => 0 } ) or $erreur = 1;
    if ($erreur) {
	erreur_dialog("Impossible de se connecter � la base de donn�e.\nV�rifiez les
 param�tres de configuration de HERBS.");
	return undef;
    }
    else {
	$sth = $bd->prepare("SELECT na, nb from evaluations where idCR=$idCR and idR
egle=$idRegle");
	$sth->execute;
	while(@row = $sth->fetchrow_array) { # un seul tuple normalement
	    $na = $row[0];
	    $nb = $row[1];
	    return ($na*$nb+$n)/($n*($na+2));
	}
    }
}

return 1;

