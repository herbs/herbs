.. -*- mode: rst -*-

===========================================
HERBS: an Enhanced Rule Benchmarking System
===========================================




Example of use
==============

(remember to use option --local if HERBS is not installed and runs from its
source directory)

::

  DB="testHERBS.db"
  
  HERBS="python ./herbscmd.py -v" # -v for info
  #HERBS="python ./herbscmd.py --local -v"
  
  # 1. Initialization
  $HERBS -c $DB

  # 2. Import dataset
  $HERBS $DB dataset import                           \
           -l buying:maint:doors:persons:lug_boot:safety:offer \
  	 --csv app/tests/data/car.data
  
  # 3. Import rules set
  $HERBS $DB rulesset import \
           --c45 -t offer app/tests/data/car.c45
  
  # 4. Show me the imported sets, and compatible couples
  # (use command: 'list --help' for more)
  $HERBS $DB list -d -r -c
  
  # 5. Computing quality measures requires that evaluations are computed before
  # When computing a quality measure, this is done automatically, however
  # it can also be done as a separate process
  # $HERBS $DB  comp -e car:car
  
  # 6. Every command has its own help available w/ option -h or --help
  # for example:
  $HERBS $DB compatibility --help

  # 7. Show available quality measures
  $HERBS $DB compatibility -q help # list possible quality measures
  
  # 8. Show evaluations for a couple (dataset, ruleset): na, nb, nab & nab_
  $HERBS $DB compatibility -s car:car
  # Note: at this point, this returns nothing if the previous step
  # (commented out in this file) named "5. Computing quality measures"
  # was not executed.


  # 9. Compute just one measure, e.g. CONF: nab/na
  $HERBS $DB compatibility -q CONF:car:car
  
  # 10. See what's computed
  $HERBS $DB list -q
  
  # 11. Compute everything
  $HERBS $DB compatibility -Q car:car
  
  # 12. show evaluations for CONF
  $HERBS $DB compatibility -S CONF:car:car


HERBS GUI
=========

The herbs GUI requires that herbs is fully installed.
To launch it: wx_herbs.py

Everything except the graphics (cf. popup-menu in the Compatibilities tab) is
available.
