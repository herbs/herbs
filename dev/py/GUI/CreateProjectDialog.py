#Boa:Frame:CreateProjectDialog

from wxPython.wx import *
from wxPython.lib.anchors import LayoutAnchors

def create(parent=None):
    return CreateProjectDialog(parent)

[wxID_PROJETNAMELABEL, wxID_PROJETNAMELABELBROWSEBUTTON, 
 wxID_PROJETNAMELABELCANCELBUTTON, wxID_PROJETNAMELABELOKBUTTON, 
 wxID_PROJETNAMELABELPROJECTNAME, wxID_PROJETNAMELABELPROJECTNAMELABEL, 
 wxID_PROJETNAMELABELSAVELABEL, 
 wxID_PROJETNAMELABELTEXTCTRL1, wxID_PROJETNAMELABELERRORLABEL,
] = map(lambda _init_ctrls: wxNewId(), range(9))

class CreateProjectDialog(wxDialog):
  def _init_ctrls(self, prnt):
    # generated method, don't edit
    wxDialog.__init__(self, style=wxDEFAULT_FRAME_STYLE,
                      name=u'projetNameLabel',
                      id=wxID_PROJETNAMELABEL,
                      parent=prnt, pos=wxPoint(229, 288),
                      size=wxSize(399, 165),
                      title=u"Project's name:")
    self.SetClientSize(wxSize(399, 165))

    self.projectNameLabel = wxStaticText(id=wxID_PROJETNAMELABELPROJECTNAMELABEL,
          label=u"Project's name:", name=u'projectNameLabel', parent=self,
          pos=wxPoint(16, 24), size=wxSize(96, 24), style=0)

    self.projectName = wxTextCtrl(id=wxID_PROJETNAMELABELPROJECTNAME,
          name=u'projectName', parent=self, pos=wxPoint(104, 24),
          size=wxSize(248, 24), style=0, value=u'')
    EVT_TEXT(self.projectName, wxID_PROJETNAMELABELPROJECTNAME,
          self.updateOkButtonStatus)

    self.SaveLabel = wxStaticText(id=wxID_PROJETNAMELABELSAVELABEL,
          label=u'Save in directory:', name=u'SaveLabel', parent=self,
          pos=wxPoint(16, 64), size=wxSize(88, 18), style=0)

    self.projectPath = wxTextCtrl(id=wxID_PROJETNAMELABELTEXTCTRL1,
          name='projectPath', parent=self,
          pos=wxPoint(112, 56), size=wxSize(200, 26), style=0, value=u'')
    EVT_TEXT(self.projectPath, wxID_PROJETNAMELABELTEXTCTRL1,
          self.updateOkButtonStatus)

    self.browseButton = wxButton(id=wxID_PROJETNAMELABELBROWSEBUTTON,
          label=u'Browse...', name=u'browseButton', parent=self,
          pos=wxPoint(320, 56), size=wxSize(72, 32), style=0)
    self.browseButton.Enable(True)
    self.browseButton.SetHelpText(u'Select a directory where the projet should be saved')
    self.browseButton.SetToolTipString(u'Select a directory where the projet should be saved')
    EVT_BUTTON(self.browseButton, wxID_PROJETNAMELABELBROWSEBUTTON,
          self.OnBrowseButton)

    self.okButton = wxButton(id=wxID_PROJETNAMELABELOKBUTTON, label=u'Ok',
          name=u'okButton', parent=self, pos=wxPoint(112, 96), size=wxSize(80,
          32), style=0)
    self.okButton.Enable(False)
    EVT_BUTTON(self.okButton, wxID_PROJETNAMELABELOKBUTTON, self.OnOkButton)

    self.cancelButton = wxButton(id=wxID_PROJETNAMELABELCANCELBUTTON,
          label=u'Cancel', name=u'cancelButton', parent=self, pos=wxPoint(224,
          96), size=wxSize(80, 32), style=0)
    EVT_BUTTON(self.cancelButton, wxID_PROJETNAMELABELCANCELBUTTON,
          self.OnCancelButton)

    self.errorLabel = wxStaticText(id=wxID_PROJETNAMELABELERRORLABEL, label=u'',
          name=u'errorLabel', parent=self, pos=wxPoint(16, 136),
          size=wxSize(368, 18), style=0)
    self.errorLabel.SetToolTipString(u'')
    self.errorLabel.SetForegroundColour(wxColour(255, 0, 0))

  def __init__(self, parent):
    self._init_ctrls(parent)

  def updateOkButtonStatus(self, event):
    import os
    ok = 1
    if not self.projectName.GetValue():
      self.errorLabel.SetLabel("The project's name cannot be empty")
      ok = 0
    elif not os.path.isdir(self.projectPath.GetValue()):
      self.errorLabel.SetLabel("The directory path is incorrect")
      ok = 0
    elif os.path.exists(os.path.join(self.projectPath.GetValue(),
                                     self.projectName.GetValue())):
      self.errorLabel.SetLabel("Error: file %s already exists"%os.path.join(self.projectPath.GetValue(), self.projectName.GetValue()))
      ok = 0
    else:
      self.errorLabel.SetLabel("")
    self.okButton.Enable(ok)

    event.Skip()

  def OnBrowseButton(self, event):
    import os
    #import pdb ; pdb.set_trace()
    defaultPath = os.path.isdir(self.projectPath.GetValue()) \
                  and os.path.abspath(self.projectPath.GetValue()) \
                  or os.path.abspath(os.path.curdir)
    dirDialog = wxDirDialog(parent=self, message="Choose a directory",
                            defaultPath=defaultPath,
                            style=wxDD_NEW_DIR_BUTTON)
    dirDialog.SetPath(defaultPath)
    try:
      r = dirDialog.ShowModal()
      if r == wxID_OK:
        self.projectPath.SetValue(dirDialog.GetPath())
    finally:
      dirDialog.Destroy()
    event.Skip()

  def OnOkButton(self, event):
    self.EndModal(wxID_OK)
    event.Skip()

  def OnCancelButton(self, event):
    self.EndModal(wxID_CANCEL)
    event.Skip()
