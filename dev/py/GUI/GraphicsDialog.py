#Boa:Dialog:GraphicsDialog

from wxPython.wx import *
import graphics_ctrl
def create(parent):
  return GraphicsDialog(parent)

[wxID_GRAPHD, wxID_GRAPHD_CANCELBUTTON, wxID_GRAPHD_DRAWGRID, 
 wxID_GRAPHD_OKBUTTON, wxID_GRAPHD_PLOTSTYLE, 
 wxID_GRAPHD_PLOTSTYLELABEL, wxID_GRAPHD_TITLELABEL, wxID_GRAPHD_XAXIS, 
 wxID_GRAPHD_XAXISLABEL, wxID_GRAPHD_XAXISLOG, wxID_GRAPHD_YAXIS, 
 wxID_GRAPHD_YAXISLABEL, wxID_GRAPHD_YAXISLOG, 
] = map(lambda _init_ctrls: wxNewId(), range(13))

from herbs.app import quality_measures

class GraphicsDialog(wxDialog):
  def _init_ctrls(self, prnt):
    # generated method, don't edit
    wxDialog.__init__(self, id=wxID_GRAPHD, name='', parent=prnt,
          pos=wxPoint(93, 641), size=wxSize(244, 262),
          style=wxDEFAULT_DIALOG_STYLE, title='Graphics Options')
    self.SetClientSize(wxSize(244, 262))

    self.titleLabel = wxStaticText(id=wxID_GRAPHD_TITLELABEL,
                                   label=u'',
                                   name=u'titleLabel', parent=self,
                                   pos=wxPoint(16, 16), size=wxSize(216, 18),
                                   style=0)

    self.xAxisLabel = wxStaticText(id=wxID_GRAPHD_XAXISLABEL,
                                   label=u'X axis:', name=u'xAxisLabel',
                                   parent=self, pos=wxPoint(24, 80),
                                   size=wxSize(35, 18), style=0)

    self.xAxis = wxComboBox(choices = self.x_choices,
                            id=wxID_GRAPHD_XAXIS, name=u'xAxis',
                            parent=self, pos=wxPoint(72, 72),
                            size=wxSize(100, 26), style=0,
                            value=self.x_choices[0])
    self.xAxis.SetLabel(u'')

    self.yAxisLabel = wxStaticText(id=wxID_GRAPHD_YAXISLABEL,
                                   label=u'Y axis:', name=u'yAxisLabel',
                                   parent=self, pos=wxPoint(24, 112),
                                   size=wxSize(35, 18), style=0)

    self.yAxis = wxComboBox(choices = self.y_choices,
                            id=wxID_GRAPHD_YAXIS, name=u'yAxis',
                            parent=self, pos=wxPoint(72, 104),
                            size=wxSize(100, 26), style=0,
                            value=self.y_choices[0])
    self.yAxis.SetLabel(u'')

    self.xAxisLog = wxCheckBox(id=wxID_GRAPHD_XAXISLOG, label=u'log scale',
                               name=u'xAxisLog', parent=self,
                               pos=wxPoint(176, 72), size=wxSize(88, 24),
                               style=0)
    self.xAxisLog.SetValue(False)

    self.yAxisLog = wxCheckBox(id=wxID_GRAPHD_YAXISLOG, label=u'log scale',
                               name=u'yAxisLog', parent=self,
                               pos=wxPoint(176, 108), size=wxSize(88, 22),
                               style=0)
    self.yAxisLog.SetValue(False)

    self.plotStyleLabel = wxStaticText(id=wxID_GRAPHD_PLOTSTYLELABEL,
                                       label=u'Plot style:',
                                       name=u'plotStyleLabel', parent=self,
                                       pos=wxPoint(16, 144),
                                       size=wxSize(50, 18), style=0)

    plotStyles = graphics_ctrl.plot_styles.keys()
    plotStyles.sort()
    self.plotStyle = wxComboBox(choices = plotStyles,
                                id=wxID_GRAPHD_PLOTSTYLE,
                                name=u'plotStyle', parent=self,
                                pos=wxPoint(72, 144), size=wxSize(150, 24),
                                style=wxCB_READONLY, value=plotStyles[0])
    self.plotStyle.SetLabel(u'')
    EVT_COMBOBOX(self.plotStyle, wxID_GRAPHD_PLOTSTYLE,
          self.OnComboBox1Combobox)

    self.drawGrid = wxCheckBox(id=wxID_GRAPHD_DRAWGRID, label=u'Draw grid',
                               name=u'drawGrid', parent=self,
                               pos=wxPoint(72, 184), size=wxSize(87, 22),
                               style=0)
    self.drawGrid.SetToolTipString(u'Whether to draw a grid or not')
    self.drawGrid.SetValue(False)

    self.okButton = wxButton(id=wxID_GRAPHD_OKBUTTON, label=u'Ok',
                             name=u'okButton', parent=self,
                             pos=wxPoint(56, 216), size=wxSize(80, 28),
                             style=0)
    EVT_BUTTON(self.okButton, wxID_GRAPHD_OKBUTTON,
               self.OnOkButton)

    self.cancelButton = wxButton(id=wxID_GRAPHD_CANCELBUTTON, label=u'Cancel',
                                 name=u'cancelButton',
                                 parent=self, pos=wxPoint(152, 216),
                                 size=wxSize(80, 28), style=0)
    EVT_BUTTON(self.cancelButton, wxID_GRAPHD_CANCELBUTTON,
               self.OnCancelButton)


  def __init__(self, parent, x_choices, y_choices):
    self.x_choices = x_choices
    self.y_choices = y_choices
    self._init_ctrls(parent)

  def OnComboBox1Combobox(self, event):
    event.Skip()

  def OnOkButton(self, event):
    self.EndModal(wxID_OK)
    event.Skip()

  def OnCancelButton(self, event):
    self.EndModal(wxID_CANCEL)
    event.Skip()

  # getters
  def x_axis(self):
    return self.xAxis.GetValue()

  def y_axis(self):
    return self.yAxis.GetValue()

  def log_x(self):
    return self.xAxisLog.IsChecked()

  def log_y(self):
    return self.yAxisLog.IsChecked()

  def grid(self):
    return self.drawGrid.IsChecked()

  def plot_style(self):
    return self.plotStyle.GetValue()
