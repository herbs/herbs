#Boa:Dialog:ImportDatasetDialog

from wxPython.wx import *
from wxPython.lib.buttons import *

import os

def create(parent):
    return ImportDatasetDialog(parent)

[wxID_IMPORTDATASETDIALOG, wxID_IMPORTDATASETDIALOGATTRIBUTESLIST, 
 wxID_IMPORTDATASETDIALOGBROWSEBTN, wxID_IMPORTDATASETDIALOGCANCELBUTTON, 
 wxID_IMPORTDATASETDIALOGCSVSEPARATOR, 
 wxID_IMPORTDATASETDIALOGCSVSEPARATORLABEL, 
 wxID_IMPORTDATASETDIALOGDATASETNAME, wxID_IMPORTDATASETDIALOGDELETEBUTTON, 
 wxID_IMPORTDATASETDIALOGDOWNBUTTON, wxID_IMPORTDATASETDIALOGDSNAMECHECKBOX, 
 wxID_IMPORTDATASETDIALOGFILENAMEFIELD, 
 wxID_IMPORTDATASETDIALOGIMPLEMENTATIONNOTE, 
 wxID_IMPORTDATASETDIALOGIMPORTFILELABEL, wxID_IMPORTDATASETDIALOGNEWBUTTON, 
 wxID_IMPORTDATASETDIALOGOKBUTTON, wxID_IMPORTDATASETDIALOGUPBUTTON, 
] = map(lambda _init_ctrls: wxNewId(), range(16))

# parent.InsertColumn(col=0, format=wxLIST_FORMAT_LEFT, heading=u'Attribute',
#                     width=parent.GetSizeTuple()[0]/2)
# parent.InsertColumn(col=1, format=wxLIST_FORMAT_LEFT, heading=u'Type',
#                     width=parent.GetSizeTuple()[0]/2)

class ImportDatasetDialog(wxDialog):
  def _init_coll_attributesList_Columns(self, parent):
    # generated method, don't edit

    parent.InsertColumn(col=0, format=wxLIST_FORMAT_LEFT, heading=u'Attribute',
          width=128)
    parent.InsertColumn(col=1, format=wxLIST_FORMAT_LEFT, heading=u'Type',
          width=128)

  def _init_ctrls(self, prnt):
    # generated method, don't edit
    wxDialog.__init__(self, id=wxID_IMPORTDATASETDIALOG,
          name='importRulesDialog', parent=prnt, pos=wxPoint(215, 198),
          size=wxSize(332, 749), style=wxDEFAULT_DIALOG_STYLE,
          title='Import a new set of rules')
    self.SetClientSize(wxSize(332, 749))

    self.attributesList = wxListCtrl(id=wxID_IMPORTDATASETDIALOGATTRIBUTESLIST,
          name=u'attributesList', parent=self, pos=wxPoint(8, 16),
          size=wxSize(256, 280), style=wxLC_REPORT)
    self.attributesList.SetToolTipString(u'List of attributes / NOT IMPLEMENTED YET!')
    self.attributesList.Enable(False)
    self._init_coll_attributesList_Columns(self.attributesList)
    EVT_LIST_ITEM_SELECTED(self.attributesList,
          wxID_IMPORTDATASETDIALOGATTRIBUTESLIST,
          self.OnAttributesList_itemSelected)

    self.newButton = wxGenButton(id=wxID_IMPORTDATASETDIALOGNEWBUTTON,
          label=u'New', name=u'newButton', parent=self, pos=wxPoint(272, 48),
          size=wxSize(48, 30), style=0)
    self.newButton.SetToolTipString(u'List of attributes / NOT IMPLEMENTED YET!')
    self.newButton.Enable(False)
    EVT_BUTTON(self.newButton, wxID_IMPORTDATASETDIALOGNEWBUTTON,
          self.OnNewButton)

    self.upButton = wxGenButton(id=wxID_IMPORTDATASETDIALOGUPBUTTON,
          label=u'Up', name=u'upButton', parent=self, pos=wxPoint(272, 104),
          size=wxSize(48, 30), style=0)
    self.upButton.SetToolTipString(u'List of attributes / NOT IMPLEMENTED YET!')
    self.upButton.Enable(False)
    EVT_BUTTON(self.upButton, wxID_IMPORTDATASETDIALOGUPBUTTON, self.OnUpButton)

    self.downButton = wxGenButton(id=wxID_IMPORTDATASETDIALOGDOWNBUTTON,
          label=u'Down', name=u'downButton', parent=self, pos=wxPoint(272, 136),
          size=wxSize(48, 30), style=0)
    self.downButton.SetToolTipString(u'List of attributes / NOT IMPLEMENTED YET!')
    self.downButton.Enable(False)
    EVT_BUTTON(self.downButton, wxID_IMPORTDATASETDIALOGDOWNBUTTON,
          self.OnDownButton)

    self.deleteButton = wxGenButton(id=wxID_IMPORTDATASETDIALOGDELETEBUTTON,
          label=u'Delete', name=u'deleteButton', parent=self, pos=wxPoint(272,
          192), size=wxSize(48, 30), style=0)
    self.deleteButton.SetToolTipString(u'List of attributes / NOT IMPLEMENTED YET!')
    self.deleteButton.Enable(False)
    EVT_BUTTON(self.deleteButton, wxID_IMPORTDATASETDIALOGDELETEBUTTON,
          self.OnDeleteButton)

    self.DSNameCheckbox = wxCheckBox(id=wxID_IMPORTDATASETDIALOGDSNAMECHECKBOX,
          label=u'Dataset Name:', name=u'DSNameCheckbox', parent=self,
          pos=wxPoint(16, 520), size=wxSize(112, 24), style=0)
    self.DSNameCheckbox.SetValue(False)
    EVT_CHECKBOX(self.DSNameCheckbox, wxID_IMPORTDATASETDIALOGDSNAMECHECKBOX,
          self.OnDSNameCheckbox)

    self.datasetName = wxTextCtrl(id=wxID_IMPORTDATASETDIALOGDATASETNAME,
          name=u'datasetName', parent=self, pos=wxPoint(136, 520),
          size=wxSize(184, 22), style=0, value='')
    self.datasetName.Enable(False)
    EVT_TEXT(self.datasetName, wxID_IMPORTDATASETDIALOGDATASETNAME,
          self.On_datasetNameText)

    self.ImportFileLabel = wxStaticText(id=wxID_IMPORTDATASETDIALOGIMPORTFILELABEL,
          label='Import file:', name='ImportFileLabel', parent=self,
          pos=wxPoint(8, 552), size=wxSize(56, 16), style=0)

    self.filenameField = wxTextCtrl(id=wxID_IMPORTDATASETDIALOGFILENAMEFIELD,
          name='filename', parent=self, pos=wxPoint(8, 570), size=wxSize(312,
          22), style=0, value='')
    EVT_TEXT(self.filenameField, wxID_IMPORTDATASETDIALOGFILENAMEFIELD,
          self.On_FilenameFieldText)

    self.BrowseBTN = wxGenButton(id=wxID_IMPORTDATASETDIALOGBROWSEBTN,
          label='Browse...', name='BrowseBTN', parent=self, pos=wxPoint(240,
          592), size=wxSize(81, 27), style=0)
    EVT_BUTTON(self.BrowseBTN, wxID_IMPORTDATASETDIALOGBROWSEBTN,
          self.On_BrowseButton)

    self.CancelButton = wxGenButton(id=wxID_IMPORTDATASETDIALOGCANCELBUTTON,
          label='Cancel', name='CancelButton', parent=self, pos=wxPoint(128,
          632), size=wxSize(81, 27), style=0)
    self.CancelButton.SetBezelWidth(2)
    self.CancelButton.SetForegroundColour(wxColour(0, 0, 0))
    EVT_BUTTON(self.CancelButton, wxID_IMPORTDATASETDIALOGCANCELBUTTON,
          self.on_CancelBTN)

    self.OkButton = wxGenButton(id=wxID_IMPORTDATASETDIALOGOKBUTTON, label='Ok',
          name='OkButton', parent=self, pos=wxPoint(16, 632), size=wxSize(81,
          27), style=0)
    self.OkButton.SetToolTipString('Import the selected file into the application')
    self.OkButton.SetForegroundColour(wxColour(0, 0, 0))
    self.OkButton.Enable(False)
    self.OkButton.SetFont(wxFont(12, wxSWISS, wxNORMAL, wxNORMAL, False,''))
    EVT_BUTTON(self.OkButton, wxID_IMPORTDATASETDIALOGOKBUTTON, self.on_OkBTN)

    self.implementationNote = wxStaticText(id=wxID_IMPORTDATASETDIALOGIMPLEMENTATIONNOTE,
          label=u"Please note: there is currently no way to specify\nthe list of attributes and their types.\n- Attributes names are extracted from\n  the first line of the CSV file\n\n- Attributes' types are guessed from\nthe CSV file (this might take some time)",
          name=u'implementationNote', parent=self, pos=wxPoint(40, 312),
          size=wxSize(280, 144), style=0)
    self.implementationNote.SetToolTipString(u'Implementation Note')

    self.csvSeparatorLabel = wxStaticText(id=wxID_IMPORTDATASETDIALOGCSVSEPARATORLABEL,
          label=u'CSV separator:', name=u'csvSeparatorLabel', parent=self,
          pos=wxPoint(16, 480), size=wxSize(77, 18), style=0)
    self.csvSeparatorLabel.SetToolTipString(u'')

    self.csvSeparator = wxTextCtrl(id=wxID_IMPORTDATASETDIALOGCSVSEPARATOR,
          name=u'csvSeparator', parent=self, pos=wxPoint(136, 472),
          size=wxSize(16, 26), style=0, value=u',')

  def __init__(self, parent):
        self._init_ctrls(parent)

  def On_RulesTypeRBSelection(self, event):
    if event.GetId()==wxID_IMPORTDATASETDIALOGAPRIORIRADIO:
          self.FullDecisionTreeCB.Enable(True)
          self.SimplifiedDecisionTreeCB.Enable(True)
          self.c45TailAttrNameLabel.Enable(False)
          self.c45TailAttrName.Enable(False)

    if event.GetId()==wxID_IMPORTDATASETDIALOGC45RADIO:
          self.FullDecisionTreeCB.Enable(False)
          self.SimplifiedDecisionTreeCB.Enable(False)
          self.c45TailAttrNameLabel.Enable(True)
          self.c45TailAttrName.Enable(True)

    self.updateOkButtonState()
    event.Skip()


  def On_BrowseButton(self, event):
    dlg = wxFileDialog(self, "Choose a file", os.getcwd(), "",
                       "CSV file (*.csv)|*.csv|All files (*.*)|*.*",
                       wxOPEN | wxCHANGE_DIR
                       )
    if dlg.ShowModal() == wxID_OK:
        self.filenameField.SetValue(dlg.GetPath())
    dlg.Destroy()
    event.Skip()

  def on_OkBTN(self, event):
    self.EndModal(wxID_OK)

  def on_CancelBTN(self, event):
    self.EndModal(wxID_CANCEL)

  def updateOkButtonState(self, event=None):
    """
    Enables the 'Ok' button iff:

      - the rules set has a name,
      - it has a valid filename
      - the tail-attribute name is specified when the format is c45

    :param event: ignored
    """
    import os
    ok = self.datasetName.GetValue()
    ok = ok and os.path.isfile(self.filenameField.GetValue())
    ok = ok and self.csvSeparator.GetValue()
    self.OkButton.Enable(bool(ok))
        
  def On_FilenameFieldText(self, event):
    ok = 1
    if not self.datasetName.GetValue() or not self.DSNameCheckbox.IsChecked():
      self.datasetName.SetValue(os.path.splitext(os.path.basename(self.filename()))[0])

    self.updateOkButtonState()
    event.Skip()

  def On_datasetNameText(self, event):
    self.updateOkButtonState()
    event.Skip()

  def OnDSNameCheckbox(self, event):
    self.datasetName.Enable(self.DSNameCheckbox.IsChecked())
    if not self.DSNameCheckbox.IsChecked() and self.filename():
      self.datasetName.SetValue(os.path.splitext(os.path.basename(self.filename()))[0])


    event.Skip()

  def name(self):
    return self.datasetName.GetValue()

  def filename(self):
    return self.filenameField.GetValue()

  def separator(self):
    return self.csvSeparator.GetValue()

  def OnAttributesList_itemSelected(self, event):
    event.Skip()

  def OnNewButton(self, event):
    event.Skip()

  def OnUpButton(self, event):
    event.Skip()

  def OnDownButton(self, event):
    event.Skip()

  def OnDeleteButton(self, event):
    event.Skip()
