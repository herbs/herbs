# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Controller for GraphicsDialog

"""
from wxPython.wx import *
from herbs.app import quality_measures

# these values should be compatible w/ the valid values for parameter
# 'marker' in matplotlib's scatter()
# see http://matplotlib.sourceforge.net/matplotlib.pylab.html#-scatter
# and herbs.app.distribution.distribution()
plot_styles = {
  ' pixel (fastest)': ',',
  'square': 's',
  'circle': 'o',
  'triangle': '^',
  'diamond': 'd',
  }

def openGraphicsDialog(frame, rs, ds, cached_quality_measures=None):
  """
  Opens the dialog window from which the user asks for a figure on a given
  couple (rules set, dataset)

  :Parameters:
    - `frame`: the parent frame, requesting the dialog
    - `rs`: the rules set's name
    - `ds`: the dataset's name
    - `cached_quality_measures`: (optional) a list of the quality measures'
      names (as strings) that have already been evaluated on (rs,ds). If
      omitted or false (None, empty list,..), then it is recomputed --see
      `quality_measures.computed_measures`
      
  """
  from GraphicsDialog import GraphicsDialog
  x_choices = [ 'na','nb','nab', 'nab_']
  
  # On large sets it can take some time to extract the set of already
  # evaluated quality measures; normally the requesting frame has already
  # computed it for display purpose, in that case we skip the computation
  # and we use the cached values
  if cached_quality_measures:
    y_choices = cached_quality_measures
  else:
    y_choices = quality_measures.computed_measures(rs, ds)
  
  y_choices.sort()
  x_choices.extend(y_choices)
  dlg=GraphicsDialog(frame, x_choices, y_choices)
  r = x_axis = y_axis = log_x = log_y = grid = None
  try:
    r = dlg.ShowModal()
    if r == wxID_OK:
      x_axis = dlg.x_axis()
      y_axis = dlg.y_axis()
      grid = dlg.grid()
      log_x, log_y = dlg.log_x(), dlg.log_y()
      plot_style = dlg.plot_style()
      
  finally:
    dlg.Destroy()

  if r == wxID_OK:
    show_graphics(frame,
                  rs, ds, x_axis, y_axis, log_x, log_y, grid, plot_style)

def show_graphics(frame,
                  rs, ds, x_axis, y_axis, log_x, log_y, grid, plot_style):
  
  import matplotlib

  # uncomment the following to use wx rather than wxagg
  #matplotlib.use('WX')
  #from matplotlib.backends.backend_wx import FigureCanvasWx as FigureCanvas
  
  # comment out the following to use wx rather than wxagg
  matplotlib.use('WXAgg')
  from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
  from matplotlib.backends.backend_wx import NavigationToolbar2Wx
  
  from matplotlib.figure import Figure
  
  class CanvasFrame(wxFrame):
      
      def __init__(self, frame):
          wxFrame.__init__(self,frame,-1,
                           'CanvasFrame',size=(550,350))
  
          self.SetBackgroundColour(wxNamedColor("WHITE"))
  
          self.figure = Figure()
          self.axes = self.figure.add_subplot(111)

      def plot(self, rs, ds, x_axis, y_axis, log_x, log_y, grid, plot_style):
          # plot
          from herbs.app import distribution
          distribution.distribution(rulesset_name=rs, dataset_name=ds,
                                    x_axis=x_axis, y_axis=y_axis,
                                    logx=log_x, logy=log_y,
                                    filename=None,
                                    show_grid=grid,
                                    plot_style=plot_style,
                                    fig=self.axes)
          self.canvas = FigureCanvas(self, -1, self.figure)
  
          self.sizer = wxBoxSizer(wxVERTICAL)
          self.sizer.Add(self.canvas, 1, wxLEFT | wxTOP | wxGROW)
          self.SetSizer(self.sizer)
          self.Fit()
  
          self.add_toolbar()  # comment this out for no toolbar
  
  
      def add_toolbar(self):
          self.toolbar = NavigationToolbar2Wx(self.canvas)
          self.toolbar.Realize()
          if wxPlatform == '__WXMAC__':
              # Mac platform (OSX 10.3, MacPython) does not seem to cope with
              # having a toolbar in a sizer. This work-around gets the buttons
              # back, but at the expense of having the toolbar at the top
              self.SetToolBar(self.toolbar)
          else:
              # On Windows platform, default window size is incorrect, so set
              # toolbar width to figure width.
              tw, th = self.toolbar.GetSizeTuple()
              fw, fh = self.canvas.GetSizeTuple()
              # By adding toolbar in sizer, we are able to put it at the bottom
              # of the frame - so appearance is closer to GTK version.
              # As noted above, doesn't work for Mac.
              self.toolbar.SetSize(wxSize(fw, th))
              self.sizer.Add(self.toolbar, 0, wxLEFT | wxEXPAND)
          # update the axes menu on the toolbar
          self.toolbar.update()  
  
          
      def OnPaint(self, event):
          self.canvas.draw()


  show = CanvasFrame(frame)
  show.plot(rs, ds, x_axis, y_axis, log_x, log_y, grid,
            plot_styles.get(plot_style, ','))
  show.Show()
