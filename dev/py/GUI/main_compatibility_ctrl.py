# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Controller for main_frame.compatibilityGrid

"""
from wxPython.wx import *
from herbs.app import rules_set, compatible
from herbs.app import IProgressBar
import main_ctrl

last_order = None
cached_couples_data = None

RULES_RANKED_K_TIMES = "Rules ranked k times in the N best rules..."
compatibility_menu_titles = [ "Informations",
                              ""
                              "Compute evaluations",
                              "Show evaluations",
                              "",
                              "Compute quality measure(s)...",
                              "Compute all quality measures",
                              "",
                              "N best rules...",
                              RULES_RANKED_K_TIMES,
                              "Graphics...",
                              ]


def reinit_ctrl():
  last_order = None
  cached_couples_data = None
  


def fill_compatibilityGrid(grid, order=None, refresh=False):
    global cached_couples_data
    assert order <= 4
    n=grid.GetNumberRows()
    grid.ClearGrid()
    if n: grid.DeleteRows(pos=0, numRows=n)
    grid.AppendRows(1)
    
    wxSafeYield()
    if main_ctrl.openedProject():
      couples = compatibilityGrid_contentSortedByColumn(grid, order, refresh)
    else:
      couples = []
      cached_couples_data = None
    wxSafeYield()

    nb_couples=len(couples)
    if nb_couples:
      grid.AppendRows(nb_couples-1)
    else:
      grid.DeleteRows(pos=0, numRows=1)
      
    idx=0

    for rs, ds, eval, cached_measures in couples:
        wxSafeYield()
        grid.SetCellValue(idx, 0, rs)
        grid.SetCellValue(idx, 1, ds)
        grid.SetCellValue(idx, 2, eval)
        grid.SetCellValue(idx, 3, ', '.join(cached_measures))
        idx+=1

def compatibilityGrid_contentSortedByColumn(grid, column=None, refresh=None):
  """

  :param column: 1 <= column <= 3

  """
  global last_order, cached_couples_data
  if refresh:
    if last_order: last_order = -last_order
    cached_couples_data=None

  couples=cached_couples_data
  if not couples:
    grid.SetCellValue(0, 0, "Getting compatible couples...")
    wxSafeYield()
    couples=compatible.compatible_couples() # (rs, ds)
    from herbs.app import evaluations, quality_measures

    _couples = []
    for c in couples:
      grid.SetCellValue(0, 0, "Checking: %s,%s"%(c[0],c[1]))
      wxSafeYield()
      _couples.append( (c[0], c[1],
                        evaluations.is_evaluated(c[0], c[1]) and 'y' or '',
                        quality_measures.computed_measures(c[0], c[1]) ) )
    couples=_couples
    
  grid.SetCellValue(0, 0, "Sorting...")
  wxSafeYield()
  sign = 1
  if last_order is not None and abs(last_order)==column:
      sign = - abs(last_order) / last_order
      last_order = - last_order 
  else:
      if column is None:
          column = 1
      last_order = column
  couples.sort(lambda x,y,col=column,sign=sign: sign * cmp(x[col-1],
                                                           y[col-1]))

  cached_couples_data = couples
  return couples

def onRightClick(frame, event):
  from utilities import wxmenus_from_menus

  frame.compatibilityGrid.SelectBlock( event.GetRow(), 0,
                                       event.GetRow(), 3, False )
  rs,ds=(frame.compatibilityGrid.GetCellValue(event.GetRow(),0),
         frame.compatibilityGrid.GetCellValue(event.GetRow(),1))
  frame.couple_clicked = (rs, ds)
  frame.compatibility_menu_ids={}
  menu=wxmenus_from_menus(compatibility_menu_titles,
                          frame.compatibility_popupMenuSelection,
                          frame.compatibility_menu_ids)
  from herbs.app import evaluations, quality_measures

  rs_ds_is_evaluated = evaluations.is_evaluated(*frame.couple_clicked)

  if rs_ds_is_evaluated:
    menu.GetMenuItems()[1].Enable(False) # compute evals
    menu.GetMenuItems()[2].Enable(True)  # Show evals
  else:
    menu.GetMenuItems()[2].Enable(False) # Show evals
    menu.GetMenuItems()[4].Enable(False) # compute quality measure
    menu.GetMenuItems()[5].Enable(False) # compute all quality measures

  if not rs_ds_is_evaluated \
     or not quality_measures.has_computed_measures(rs,ds):
    menu.GetMenuItems()[7].Enable(False) # N best rules
    menu.GetMenuItems()[8].Enable(False) # Rules ranked [...]
    menu.GetMenuItems()[9].Enable(False) # Graphics...
     
  frame.PopupMenu( menu, event.GetPosition() )

def popupMenuSelection(frame, operation, rs, ds):
  from herbs.utils import log
  log.debug("popMenuSelection(): operation: %s on %s:%s", operation, rs, ds)

  # set this to True if a given operation requires a refresh
  refresh_grid = False

  wxBeginBusyCursor()
  try:

    if operation == "Informations":
      from herbs.app import compatible
      info = "Rules set: %s / Dataset: %s\n\n"%(rs, ds)
      info += "Taux de couverture: %f%%\n"%(compatible.taux_couverture(rs, ds)*100)
      dlg = wxMessageDialog( frame,
                             info,
                             'Info.',
                             wxOK )
      try:
          dlg.ShowModal()
      finally:
          dlg.Destroy()
    
    elif operation == "Compute evaluations":
      pb=ProgressBar(frame, "Computing evaluation for %s:%s"%(rs,ds))
      from herbs.app import evaluations
      evaluations.evaluate(rs, ds, pb)
      pb.finalize()
      refresh_grid = True
    
    elif operation == "Show evaluations":
      import SetFrame
      dlg=SetFrame.create(frame)
      dlg.init_with_compatible_couple(rs, ds)
      dlg.Show()


    elif operation == "Compute quality measure(s)...":
      from herbs.app import quality_measures
      measures = quality_measures.measures.keys()
      measures.sort()
      choices = wxMultiChoiceDialog(frame,
                                    "Choose one or more quality measures",
                                    "Compute quality measure(s)", #caption
                                    measures)
      choices.SetSelections([])
      if choices.ShowModal() != wxID_OK:
        return
      choices = [measures[i] for i in choices.GetSelections()]

      last_idx = len(choices) - 1
      for i, choice in enumerate(choices):
        pb = ProgressBar(frame, "Computing quality measure: %s for %s:%s"%(choice,rs,ds), auto_hide=(i!=last_idx))
        quality_measures.evaluate(choice, rs, ds, progressBar=pb)
        pb.finalize()
      refresh_grid = True

        
    elif operation == "Compute all quality measures":
      pb=ProgressBar(frame, "Computing all quality measures for %s:%s"%(rs,ds))
      from herbs.app import quality_measures
      quality_measures.evaluate_all(rs, ds, progressBar=pb)
      pb.finalize()
      refresh_grid = True
    
      
    elif operation in ( "N best rules...", RULES_RANKED_K_TIMES):
      # Dev. note to make some point in the code below clearer:
      # if RULES_RANKED_K_TIMES:
      #   - rules_ranked_k is True
      #   - 'measure' is a list of the measures chosen, while 'measures'
      #     remains the list of the valid measures

      from wx_utils import TextFieldValidator, DIGIT_ONLY
      from herbs.app import rules_set

      rules_ranked_k = (operation==RULES_RANKED_K_TIMES)

      # Get N...
      max_N = rules_set.nb_rules(rs)
      N = wxTextEntryDialog(frame, "(1<=N<=%li) N:"%max_N, "Enter N", "1")
      _text_field=[c for c in N.GetChildren() if isinstance(c, wxTextCtrl)][0]
      to_N = lambda value, N=N: N.SetValue(value)
      validator = TextFieldValidator(DIGIT_ONLY,
                                     bounds=[1, max_N],
                                     transfer_from_window = to_N)
      _text_field.SetValidator(validator)
      if N.ShowModal() != wxID_OK:
        return
      N = int(N.GetValue())

      # ... and the measure
      from herbs.app import quality_measures
      #measures = quality_measures.computed_measures(rs, ds)
      measures = ([i[3] for i in cached_couples_data
                   if i[:2]==(rs,ds)]+[()])[0]
      measures.sort()
      if not rules_ranked_k:
        measure = wxSingleChoiceDialog(frame,
                                       "Choose one quality measure",
                                       "N best values", #caption
                                       measures)
      else:
        measure = wxMultiChoiceDialog(frame,
                                      "Choose one or more quality measures",
                                      RULES_RANKED_K_TIMES, #caption
                                      measures)
        measure.SetSelections([])
        measure.Fit()
      if measure.ShowModal() != wxID_OK:
        return

      if not rules_ranked_k:
        measure = measures[measure.GetSelection()]
      else:
        measure = [measures[i] for i in measure.GetSelections()]
        
      # ...and k...
      k = None
      if rules_ranked_k:
        max_k = len(measure)
        k = wxTextEntryDialog(frame, "(1<=k<=%li) k:"%max_k, "Enter k", "1")
        _text_field=[c for c in k.GetChildren() if isinstance(c,wxTextCtrl)][0]
        to_k = lambda value, k=k: k.SetValue(value)
        validator = TextFieldValidator(DIGIT_ONLY,
                                       bounds=[1, max_k],
                                       transfer_from_window = to_k)
        _text_field.SetValidator(validator)
        if k.ShowModal() != wxID_OK:
          return
        k = int(k.GetValue())

      
      # ...and finally display the result
      import SetFrame
      dlg=SetFrame.create(frame)
      # this method handles both cases, depending on whether k is None
      dlg.init_with_N_best_rules(rs, ds, measure, N, k)
      dlg.Show()

      
    elif operation == "Graphics...":
      from graphics_ctrl import openGraphicsDialog
      openGraphicsDialog(frame, rs, ds,
                         ([i[3] for i in cached_couples_data
                           if i[:2]==(rs,ds)]+[()])[0])
  finally:
    wxEndBusyCursor()
    
  # the following is not included in the previous try/finally block because
  # the progress bar resets the cursor to the standard one when reaching 100%
  wxBeginBusyCursor()
  try:
    if refresh_grid:
      fill_compatibilityGrid(frame.compatibilityGrid, refresh=True)
  finally:
    wxEndBusyCursor()
    


class ProgressBar(IProgressBar.ProgressBarBase):
  wxbar = None

  def __init__(self, parentWindow, msg, auto_hide=False):
    self.parent=None
    self.msg=msg
    self.auto_hide = auto_hide
    
  def setValue(self, value):
    if self.wxbar==None:
      style = wxPD_CAN_ABORT | wxPD_APP_MODAL | wxPD_ELAPSED_TIME \
              | wxPD_ESTIMATED_TIME | wxPD_REMAINING_TIME
      if self.auto_hide:
        style |= wxPD_AUTO_HIDE
      self.wxbar=wxProgressDialog("Evaluation",
                                  self.msg,
                                  maximum=self.maximum, parent=self.parent,
                                  style=style)

    return self.wxbar.Update(value)

  def finalize(self):
    if self.wxbar:
      self.wxbar.Destroy()


