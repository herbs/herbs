#Boa:Frame:wxFrame1

from wxPython.wx import *
from wxPython.grid import *
from wxPython.lib.anchors import LayoutAnchors

import os, sys
sys.path.append(os.getcwd())

import main_ctrl, main_compatibility_ctrl
from utilities import *

rules_menu_titles = [ #{"Import a new rule set":
                      # [ "C4.5",
                      #   { "apriori":
                      #     [ "Full Decision Tree",
                      #       "Simplified Decision Tree"]
                      #    }
                      #   ]
                      # },
                      "Informations",
                      "Display the whole rules set",
                      "",
                      "Rename",
                      "",
                      "Delete" ]
datasets_menu_titles = [ "Informations",
                         "Display the whole dataset",
                         "",
                         "Rename",
                         "",
                         "Delete" ]

def create(parent):
    return wxMainFrame(parent)


[wxID_WXFRAME1, wxID_WXFRAME1DATASETSGRID, wxID_WXFRAME1NOTEBOOK1, 
 wxID_WXFRAME1RULESGRID, wxID_WXFRAME1COMPATIBILITYGRID,
 wxID_WXFRAME1STATUSBAR1, 
] = map(lambda _init_ctrls: wxNewId(), range(6))

[wxID_WXFRAME1MENU2ABOUT_MI, wxID_WXFRAME1MENU2FILE_MI,
] = map(lambda _init_coll_menu2_Items: wxNewId(), range(2))

[wxID_FILE__NEW, wxID_FILE__OPEN_PRJ, wxID_FILE__CLOSE_PRJ,
 wxID_FILE__RECENT_FILES, wxID_FILE__EXIT
] = map(lambda _init_coll_file_menu_Items: wxNewId(), range(5))

[wxID_WXFRAME1DATASETS_MENUDATASETS_EXPORT_MI, 
 wxID_WXFRAME1DATASETS_MENUDATASETS_IMPORT_MI, 
 wxID_WXFRAME1DATASETS_MENUDATASETS_INSPECT_MI, 
 wxID_WXFRAME1DATASETS_MENUDATASETS_REMOVE_MI, 
 wxID_WXFRAME1DATASETS_MENUDATASETS_RENAME_MI, 
] = map(lambda _init_coll_datasets_menu_Items: wxNewId(), range(5))

[wxID_WXFRAME1RULESSETS_MENURULESSETS_EXPORT_MENU, 
 wxID_WXFRAME1RULESSETS_MENURULESSETS_IMPORT_MI, 
 wxID_WXFRAME1RULESSETS_MENURULESSETS_INSPECT_MI, 
 wxID_WXFRAME1RULESSETS_MENURULESSETS_REMOVE_MI, 
 wxID_WXFRAME1RULESSETS_MENURULESSETS_RENAME_MI, 
] = map(lambda _init_coll_rulessets_menu_Items: wxNewId(), range(5))

[wxID_WXFRAME1ABOUT_MENUABOUT_MI] = map(lambda _init_coll_about_menu_Items: wxNewId(), range(1))



class wxMainFrame(wxFrame):
  
  def _init_coll_main_menuBar_Menus(self, parent):
    # generated method, don't edit

    parent.Append(menu=self.file_menu, title='&File')
    parent.Append(menu=self.rulessets_menu, title=u'&Rules Sets')
    parent.Append(menu=self.datasets_menu, title=u'&Datasets')
    parent.Append(menu=self.about_menu, title=u'&About')

  def _init_coll_datasets_menu_Items(self, parent):
    # generated method, don't edit

    parent.Append(help='',
          id=wxID_WXFRAME1DATASETS_MENUDATASETS_IMPORT_MI, text=u'Import',
          kind=wxITEM_NORMAL)
    parent.Append(help='',
          id=wxID_WXFRAME1DATASETS_MENUDATASETS_EXPORT_MI, text=u'Export',
          kind=wxITEM_NORMAL)
    #parent.Append(help='',
    #      id=wxID_WXFRAME1DATASETS_MENUDATASETS_REMOVE_MI, text=u'Remove',
    #      kind=wxITEM_NORMAL)
    #parent.Append(help='',
    #      id=wxID_WXFRAME1DATASETS_MENUDATASETS_RENAME_MI, text=u'Rename',
    #      kind=wxITEM_NORMAL)
    #parent.Append(help='',
    #      id=wxID_WXFRAME1DATASETS_MENUDATASETS_INSPECT_MI,
    #      text=u'Inspect / Show Details', kind=wxITEM_NORMAL)

    EVT_MENU(self, wxID_WXFRAME1DATASETS_MENUDATASETS_IMPORT_MI,
             self.OnDataset_import)
    EVT_MENU(self, wxID_WXFRAME1DATASETS_MENUDATASETS_EXPORT_MI,
             self.unimplemented_dialog)

  def onMenuFile_newProject(self, event):
    from herbs.app import conn
    if conn.current_dbname:
      dlg = wxMessageDialog(
        self, 'Are you sure you want to close the current project and '\
        'create a new one?',
        'Create a new project', wxYES_NO | wxYES_DEFAULT | wxICON_QUESTION)
      try:
        r=dlg.ShowModal()
      finally:
        dlg.Destroy()
        
      if r!=wxID_YES:
        return

    import CreateProjectDialog
    d = CreateProjectDialog.create(self)
    r = None
    try:
      r = d.ShowModal()
    finally:
      d.Destroy()

    if r == wxID_OK:
      file = os.path.join(d.projectPath.GetValue(), d.projectName.GetValue())
      main_ctrl.openProject(self, file, create=True)
    event.Skip()

  def onMenuFile_openProject(self, event):
    from herbs.app import conn
  
    if conn.current_dbname:
      dlg = wxMessageDialog(
        self, 'Are you sure you want to close the current project and '\
        ' open a new one?',
        'Open a new project', wxYES_NO | wxYES_DEFAULT | wxICON_QUESTION)
      try:
        r=dlg.ShowModal()
      finally:
        dlg.Destroy()
        
      if r!=wxID_YES:
        return

    wxBeginBusyCursor()
    try:
      dlg = wxFileDialog(self,"Nom du projet","","","*.db;*.hrb",wxOPEN)
      r = dlg.ShowModal()
      if r != wxID_OK :
        return
      
      file = dlg.GetPath()
      main_ctrl.openProject(self,file)
      event.Skip()
    finally:
      wxEndBusyCursor()
      
  def onMenuFile_closeProject(self, event):
    from herbs.app import conn
    if conn.current_dbname:
      conn.init(None)

    main_ctrl.onCloseProject(self)
    
    event.Skip()

  def onMenuFile_exit(self, event):
        dlg = wxMessageDialog(
          self, 'Are you sure you want to quit the application?',
          'Caption', wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION)
        try:
            r=dlg.ShowModal()
        finally:
            dlg.Destroy()

        if r==wxID_YES:
            self.Close()

  def onMenuFile_recentFiles(self, event):
    file = self.ids_recent_files[event.GetId()]
    from herbs.app import conn
  
    if conn.current_dbname and conn.current_dbname  == file:
      return
    
    if conn.current_dbname:
      dlg = wxMessageDialog(
        self, 'Are you sure you want to close the current project and '\
        ' open %s?'%file,
        'Open a new project', wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION)
      try:
        r=dlg.ShowModal()
      finally:
        dlg.Destroy()
        
      if r!=wxID_YES:
        return

    wxBeginBusyCursor()
    try:
        main_ctrl.openProject(self, file)
    finally:
        wxEndBusyCursor()
    event.Skip()

  def _init_recent_file(self, recent_files):
    self.recents=recent_files
    self.menu_recent_files_ids = [wxNewId() for r in self.recents]
    self.menu_recent_files_struct = { }

    self.ids_recent_files = {}
    [self.ids_recent_files.setdefault(id,item)
     for id,item in map(None, self.menu_recent_files_ids, self.recents)]

    for id in self.menu_recent_files_ids:
      path=self.ids_recent_files[id]
      self.menu_recent_files_struct[id] = {
        'init': { 'text': path,
                  'kind': wxITEM_NORMAL },
        'action': self.onMenuFile_recentFiles,
        }

    self.menu_recent_files = create_menu(self, '',
                                         self.menu_recent_files_ids,
                                         self.menu_recent_files_struct)

    
  def _init_menu_file(self):

    #self._init_recent_file(['/home/big/Projets/HERBS/dev/py/test.db',
    #                        '/home/big/Projets/HERBS/dev/test.db'])
    self.menu_file_ids = [ wxID_FILE__NEW,
                           wxID_FILE__OPEN_PRJ, wxID_FILE__CLOSE_PRJ,
                           #wxID_FILE__RECENT_FILES,
                           -1, wxID_FILE__EXIT]    

    self.menu_file = {
      wxID_FILE__NEW: { 'init': { 'help': 'Create a new project',
                                  'text': '&New Project...',
                                  'kind': wxITEM_NORMAL },
                        'action': self.onMenuFile_newProject },
      
      wxID_FILE__OPEN_PRJ: { 'init': { 'help': 'Open an existing project',
                                       'text': '&Open project...\tCtrl-O',
                                       'kind': wxITEM_NORMAL },
                             'action': self.onMenuFile_openProject },
      
      wxID_FILE__OPEN_PRJ: { 'init': { 'help': 'Open an existing project',
                                       'text': '&Open project...\tCtrl-O',
                                       'kind': wxITEM_NORMAL },
                             'action': self.onMenuFile_openProject },
      
      wxID_FILE__CLOSE_PRJ: { 'init': { 'text': 'Close project\tCtrl-W',
                                        'kind': wxITEM_NORMAL,
                                        'help': 'Stop working on current project',
                                        },
                              'action': self.onMenuFile_closeProject },

      #wxID_FILE__RECENT_FILES: { 'init': { 'text': 'Recent files...',
      #                                     'help': 'Recently opened projects',
      #                                  },
      #                           'submenu': self.menu_recent_files },
      
      wxID_FILE__EXIT: { 'init': { 'text': '&Exit\tCtrl-Q',
                                   'help': 'Exit the application', 
                                   'kind': wxITEM_NORMAL },
                         'action': self.onMenuFile_exit },
      }
    
    menu = create_menu(self, '', self.menu_file_ids, self.menu_file)
    return menu
  
  def _init_coll_rulessets_menu_Items(self, parent):
    # generated method, don't edit

    parent.Append(help='',
          id=wxID_WXFRAME1RULESSETS_MENURULESSETS_IMPORT_MI, text=u'Import',
          kind=wxITEM_NORMAL)
    parent.Append(help='',
          id=wxID_WXFRAME1RULESSETS_MENURULESSETS_EXPORT_MENU, text=u'Export',
          kind=wxITEM_NORMAL)
    #parent.Append(help='',
    #      id=wxID_WXFRAME1RULESSETS_MENURULESSETS_REMOVE_MI, text=u'Remove',
    #      kind=wxITEM_NORMAL)
    #parent.Append(help='',
    #      id=wxID_WXFRAME1RULESSETS_MENURULESSETS_RENAME_MI, text=u'Rename',
    #      kind=wxITEM_NORMAL)
    #parent.Append(help='',
    #              id=wxID_WXFRAME1RULESSETS_MENURULESSETS_INSPECT_MI,
    #              text=u'Inspect / Show Details', kind=wxITEM_NORMAL)
    EVT_MENU(self, wxID_WXFRAME1RULESSETS_MENURULESSETS_IMPORT_MI,
             self.OnRulessets_import)

    EVT_MENU(self, wxID_WXFRAME1RULESSETS_MENURULESSETS_EXPORT_MENU,
             self.unimplemented_dialog)

  def _init_coll_about_menu_Items(self, parent):
    # generated method, don't edit

    parent.Append(help='About herbs', id=wxID_WXFRAME1ABOUT_MENUABOUT_MI,
                  text='About', kind=wxITEM_NORMAL)
      
  def _init_coll_notebook1_Pages(self, parent):
    # generated method, don't edit

    parent.AddPage(imageId=-1, page=self.rulesGrid, select=True, text='Rules')
    parent.AddPage(imageId=-1, page=self.datasetsGrid, select=False,
                   text='Datasets')
    parent.AddPage(imageId=-1, page=self.compatibilityGrid, select=False,
                   text='Compatible couples')

  def _init_utils(self):
    # generated method, don't edit
    self.main_menuBar = wxMenuBar()
 
    self.file_menu = self._init_menu_file()

    self.about_menu = wxMenu(title='')

    self.rulessets_menu = wxMenu(title='')

    self.datasets_menu = wxMenu(title=u'')

    self._init_coll_about_menu_Items(self.about_menu)
    self._init_coll_rulessets_menu_Items(self.rulessets_menu)
    self._init_coll_datasets_menu_Items(self.datasets_menu)
    self._init_coll_main_menuBar_Menus(self.main_menuBar)

    #self.menu_file[wxID_FILE__OPEN_PRJ]['item'].Enable(False)
      
  def _init_ctrls(self, prnt):
    # generated method, don't edit
    wxFrame.__init__(self, id=wxID_WXFRAME1, name='', parent=prnt,
          pos=wxPoint(183, 530), size=wxSize(800, 516),
          style=wxDEFAULT_FRAME_STYLE, title=u'HERBS')
    self._init_utils()
    self.SetClientSize(wxSize(800, 516))
    self.SetMenuBar(self.main_menuBar)

    self.statusBar = wxStatusBar(id=wxID_WXFRAME1STATUSBAR1, name='statusBar',
          parent=self, style=0)
    self.SetStatusBar(self.statusBar)

    self.main_notebook = wxNotebook(id=wxID_WXFRAME1NOTEBOOK1,
          name='main_notebook', parent=self, pos=wxPoint(0, 0),
          size=wxSize(800, 463), style=0)

    EVT_NOTEBOOK_PAGE_CHANGED(self.main_notebook, wxID_WXFRAME1NOTEBOOK1,
          self.OnNotebook1NotebookPageChanged)

    self.rulesGrid = wxGrid(id=wxID_WXFRAME1RULESGRID, name='rulesGrid',
          parent=self.main_notebook, pos=wxPoint(0, 0), size=wxSize(796, 427),
          style=wxHSCROLL|wxVSCROLL)
    
    self.rulesGrid.SetRowLabelSize(0)
    self.rulesGrid.EnableEditing(False)
    self.rulesGrid.EnableGridLines(False)
    EVT_GRID_CELL_RIGHT_CLICK(self.rulesGrid,
          self.OnRulesgridGridCellRightClick)
    self.rulesGrid.AdjustScrollbars()
    EVT_GRID_COL_SIZE(self.rulesGrid, self.gridIsResized)

    self.datasetsGrid = wxGrid(id=wxID_WXFRAME1DATASETSGRID,
          name='datasetsGrid', parent=self.main_notebook, pos=wxPoint(0, 0),
          size=wxSize(796, 427), style=wxHSCROLL|wxVSCROLL)
    self.datasetsGrid.SetColLabelSize(20)
    self.datasetsGrid.SetRowLabelSize(0)
    self.datasetsGrid.EnableEditing(False)
    self.datasetsGrid.SetDefaultColSize(160)
    self.datasetsGrid.EnableGridLines(False)
    EVT_GRID_CELL_RIGHT_CLICK(self.datasetsGrid,
          self.OnDatasetsgridGridCellRightClick)
    self.datasetsGrid.AdjustScrollbars()
    EVT_GRID_COL_SIZE(self.datasetsGrid, self.gridIsResized)

    self.compatibilityGrid = wxGrid(id=wxID_WXFRAME1COMPATIBILITYGRID,
          name='compatibilityGrid', parent=self.main_notebook,
          pos=wxPoint(0,0), size=wxSize(796, 427), style=wxHSCROLL|wxVSCROLL)
    
    self.compatibilityGrid.SetColLabelSize(20)
    self.compatibilityGrid.SetRowLabelSize(0)
    self.compatibilityGrid.EnableEditing(False)
    self.compatibilityGrid.SetDefaultColSize(160)
    self.compatibilityGrid.EnableGridLines(False)
    EVT_GRID_CELL_RIGHT_CLICK(self.compatibilityGrid,
                              self.OnCompatibilityGridCellRightClick)
    EVT_GRID_LABEL_LEFT_CLICK(self.compatibilityGrid,
                              self.OnCompatibilityGridLabelLeftClick)
    self.compatibilityGrid.AdjustScrollbars()
    EVT_GRID_COL_SIZE(self.compatibilityGrid, self.gridIsResized)

    self._init_coll_notebook1_Pages(self.main_notebook)

  def gridIsResized(self, event):
      grid = event.GetEventObject()
      grid.AdjustScrollbars()
      
  def __init__(self, parent):
        self._init_ctrls(parent)
        #self.scrolledWindow1.SetScrollbars(20,20,50,50)
        #self.scrolledWindow2.SetScrollbars(20,20,50,30)
        
        self.rulesGrid.CreateGrid(0,2)
        self.rulesGrid.SetColLabelValue(0, 'Name')
        self.rulesGrid.SetColLabelValue(1, 'Description')
        w, h = self.rulesGrid.GetSizeTuple()
        self.rulesGrid.SetColSize(0, w/2)
        self.rulesGrid.SetColSize(1, w/2)

        self.datasetsGrid.CreateGrid(0,2)
        self.datasetsGrid.SetColLabelValue(0, 'Name')
        self.datasetsGrid.SetColLabelValue(1, 'Description')
        w, h = self.datasetsGrid.GetSizeTuple()
        self.datasetsGrid.SetColSize(0, w/2)
        self.datasetsGrid.SetColSize(1, w/2)

        self.compatibilityGrid.CreateGrid(0,4)
        self.compatibilityGrid.SetColLabelValue(0, 'Rules set')
        self.compatibilityGrid.SetColLabelValue(1, 'Dataset')
        self.compatibilityGrid.SetColLabelValue(2, 'evaluation')
        self.compatibilityGrid.SetColFormatBool(2)
        self.compatibilityGrid.SetColLabelValue(3, 'Quality measures (cached)')
        #w, h = self.compatibilityGrid.GetSizeTuple()
        #self.compatibilityGrid.SetColSize(0, w/4)
        #self.compatibilityGrid.SetColSize(1, w/4)
        #self.compatibilityGrid.SetColSize(2, w/4)
        #self.compatibilityGrid.SetColSize(3, w/4)

        # TBD try to center content in this column
        #cell_attr=wxGridCellAttr()
        ##self.compatibilityGrid.GetTable().GetAttr(2,2).Clone()
        #cell_attr.SetAlignment(wxALIGN_CENTER)
        #self.compatibilityGrid.SetColAttr(2, cell_attr)

        # Do this here, no in _init_ctrls: if an argument is given (==a project
        # to load), the following will try to fill the grids with data... and
        # this fails because wxGrid.AppendRows() can't be called before
        # wxGrid.CreateGrid()
        main_ctrl.onOpenProject(self,main_ctrl.isaProjectOpened())
        
  def OnNotebook1NotebookPageChanged(self, event):
    if event.GetSelection()==0:
      main_ctrl.fill_rulesGrid(self)
    elif event.GetSelection()==1:
      main_ctrl.fill_datasetsGrid(self)
    elif event.GetSelection()==2:
      main_compatibility_ctrl.fill_compatibilityGrid(self.compatibilityGrid)
    else:
      pass

    event.Skip()


  ##
  ## Rules Grid
  def OnRulesgridGridCellRightClick(self, event):
        self.rulesGrid.SelectBlock(event.GetRow(), 0,
                                   event.GetRow(), 1,False)
        self.rule_clicked = self.rulesGrid.GetCellValue(event.GetRow(), 0)

        ### 2. Launcher creates wxMenu. ###
        #menu = wxMenu()
        #for (id,title) in rules_menu_title_by_id.items():
        #    ### 3. Launcher packs menu with Append. ###
        #    menu.Append( id, title )
        #    ### 4. Launcher registers menu handlers with EVT_MENU, on the menu. ###
        #    EVT_MENU( menu, id, self.rules_popupMenuSelection )

        ### 5. Launcher displays menu with call to PopupMenu,
        ##     invoked on the source component, passing event's GetPoint. ###
        self.rules_menu_ids={}
        menu=wxmenus_from_menus(rules_menu_titles,
                                self.rules_popupMenuSelection, self.rules_menu_ids)

        self.PopupMenu( menu, event.GetPosition() )
        event.Skip()


  def rules_popupMenuSelection( self, event ):
        operation = self.rules_menu_ids[ event.GetId() ]
        target    = self.rule_clicked
        #print 'Perform "%(operation)s" on "%(target)s."' % vars()
        if operation=='Informations':
            from herbs.app import rules_set
            info = "Rules set: %s\n\n"%target
            info += "Nb of rules: %li\n"%rules_set.nb_rules(target)
            info += "Attributes: \n"
            for attr in rules_set.attributes(target):
                info += "  - "+attr+"\n"
            dlg = wxMessageDialog( self,
                                   info,
                                   'Info.',
                                   wxOK )
            try:
                dlg.ShowModal()
            finally:
                dlg.Destroy()

        elif operation=='Display the whole rules set':
            import SetFrame
            dlg=SetFrame.create(self, rs_name=self.rule_clicked)
            dlg.Show()

        elif operation=='Delete':
            dlg = wxMessageDialog(
              self, 'Are you sure you want to definitely delete the rule\n%s?'%self.rule_clicked,
              'Rule deletion', wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION)
            try:
                r=dlg.ShowModal()
            finally:
                dlg.Destroy()

            if r==wxID_YES:
              main_ctrl.delete_rs(self, self.rule_clicked)

        elif operation=='Rename':

          dlg = wxTextEntryDialog(self, "Enter the new name for %s: "%target,
                                  caption = "Rename a rules set",
                                  defaultValue = target,
                                  style = wxOK | wxCANCEL | wxCENTRE
                                  )
          r=None
          try:
            r=dlg.ShowModal()
            new_name=dlg.GetValue()
          finally:
            dlg.Destroy()

          if r==wxID_OK:
            main_ctrl.rename_rs(self, target, new_name)

  ## Datasets grid
  def OnDatasetsgridGridCellRightClick(self, event):
      self.datasetsGrid.SelectBlock(event.GetRow(), 0,
                                    event.GetRow(), 1,False)
      self.dataset_clicked = self.datasetsGrid.GetCellValue(event.GetRow(), 0)

      self.datasets_menu_ids={}
      menu=wxmenus_from_menus(datasets_menu_titles,
                              self.dataset_popupMenuSelection,
                              self.datasets_menu_ids)
      
      self.PopupMenu( menu, event.GetPosition() )
      event.Skip()

  def dataset_popupMenuSelection( self, event ):
      operation = self.datasets_menu_ids[ event.GetId() ]
      target    = self.dataset_clicked
      #print 'Perform "%(operation)s" on "%(target)s."' % vars()
      if operation=='Informations':
            from herbs.app import dataset
            info = "Dataset: %s\n\n"%target
            info += "Nb of rows: %li\n"%dataset.nb_instances(target)
            info += "Attributes (and their types): \n"
            for attr in dataset.attributes(target):
                info += "  - %s (%s)\n"%attr
            dlg = wxMessageDialog( self,
                                   info,
                                   'Info.',
                                   wxOK )
            try:
                dlg.ShowModal()
            finally:
                dlg.Destroy()

      elif operation=='Display the whole dataset':
          import SetFrame
          dlg=SetFrame.create(self, ds_name=self.dataset_clicked)
          dlg.Show()

      elif operation=='Delete':
          dlg = wxMessageDialog(
            self, 'Are you sure you want to definitely delete the dataset\n%s?'%self.dataset_clicked,
            'Dataset deletion', wxYES_NO | wxNO_DEFAULT | wxICON_QUESTION)
          try:
              r=dlg.ShowModal()
          finally:
              dlg.Destroy()

          if r==wxID_YES:
            main_ctrl.delete_ds(self, self.dataset_clicked)

      elif operation=='Rename':
        self.unimplemented_dialog()

      event.Skip()
      
  ## Compatibility grid
  def OnCompatibilityGridCellRightClick(self, event):
      main_compatibility_ctrl.onRightClick(self, event)
      event.Skip()

  def OnCompatibilityGridLabelLeftClick(self, event):
      #self.compatibilityGrid.SelectBlock(event.GetRow(), 0,
      #                                     event.GetRow(), 1,False)
      #self.rule_clicked = self.compatibilityGrid.GetCellValue(event.GetRow(),
      #                                                          0)
      main_compatibility_ctrl.fill_compatibilityGrid( self.compatibilityGrid,
                                                      event.GetCol() + 1 )

  def compatibility_popupMenuSelection( self, event ):
      operation = self.compatibility_menu_ids[ event.GetId() ]
      target    = self.couple_clicked
      main_compatibility_ctrl.popupMenuSelection(self, operation, *target)
      

  def OnRulessets_import(self, event):
      from importRulesDialog import importRulesDialog
      dlg=importRulesDialog(self)
      name = filename = format = c45_attr = None

      try:
        res=dlg.ShowModal()
        name, filename = dlg.name(), dlg.filename()
        format, c45_attr= dlg.format(), dlg.c45_tailAttrName()
      finally:
        dlg.Destroy()

      if res!=wxID_OK:
        return

      try:
        main_ctrl.import_rs(self, name, filename, format , c45_attr)
      except Exception, e:
        dlg = wxMessageDialog( self, 'Error: '+e.args[0],
                               'Error importing the rules set',
                               wxOK | wxICON_ERROR )
        dlg.ShowModal()
        dlg.Destroy()

  def OnDataset_import(self, event):
      from ImportDatasetDialog import ImportDatasetDialog
      dlg=ImportDatasetDialog(self)

      name = filename = None

      try:
        res=dlg.ShowModal()
        name, filename, separator = dlg.name(), dlg.filename(), dlg.separator()
      finally:
        dlg.Destroy()

      if res!=wxID_OK:
        return

      try:
        main_ctrl.import_ds(self, name, filename, separator)
      except Exception, e:
        dlg = wxMessageDialog( self, 'Error: '+e.args[0],
                               'Error importing the rules set',
                               wxOK | wxICON_ERROR )
        dlg.ShowModal()
        dlg.Destroy()

  def unimplemented_dialog(self, event=None):
      dlg = wxMessageDialog(self, "Unimplemented", "Unimplemented",
                            style = wxOK | wxICON_EXCLAMATION)
      try:
          dlg.ShowModal()
      finally:
          dlg.Destroy()
          
      if event:
          event.Skip()
