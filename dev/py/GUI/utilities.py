# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Utilities for building GUIs w/ wxPython
"""
from wxPython.wx import *

def wxmenus_from_menus(menu_titles, handler, items_ids):
    wx_menu = wxMenu()
    for title in menu_titles:
        if type(title) is type({}):
            if len(title)!=1: raise ValueError, "len(dictionary)!=1 in menu_titles"
            _title, _menu = title.items()[0]
            wx_menu.AppendMenu( wxNewId(), _title,
                                wxmenus_from_menus(_menu, handler, items_ids))
        else:
            if title:
                id=wxNewId()
                items_ids[id]=title
                wx_menu.Append( id, title )
                EVT_MENU( wx_menu, id, handler )
            else:
                wx_menu.AppendSeparator()
    return wx_menu

def create_menu(frame, title, menu_ids, menu_struct, menu=None):
  """
  TBD Documentation forthcoming
  """
  # Please note: wxMenuItem() documentation says 'helpString' is an optional
  # parameter, but it only accepts 'help'
  _default_menu_Append_call_args={'kind':'wxITEM_NORMAL','help':''}
  _default_menu_AppendMenu_call_args={'help':''}

  if menu is None:
    menu = wxMenu(title=title)
  
  for id in menu_ids:
    if id == -1:
      item = wxMenuItem(menu, id = wxID_SEPARATOR,
                        text="", kind=wxITEM_SEPARATOR, help="")
      menu.AppendItem( item )
      #menu.Append( id = wxID_SEPARATOR,
      #             item="", kind=wxITEM_SEPARATOR, helpString="" )
    else:
      kwargs = None

      if menu_struct[id].has_key('submenu'):
        kwargs = _default_menu_AppendMenu_call_args.copy()
        kwargs.update(menu_struct[id]['init'])
        if 'kind' in kwargs:
          raise ValueError, "no 'kind' allowed for submenus"
        submenu = menu_struct[id]['submenu']
        item = wxMenuItem(menu, id = id, subMenu=submenu, **kwargs)
        menu.AppendItem( item )

      else: # normal item
        kwargs = _default_menu_Append_call_args.copy()
        kwargs.update(menu_struct[id]['init'])
        item = wxMenuItem(menu, id = id, **kwargs)
        menu.AppendItem( item )

      #menu.Append( id=id, **kwargs )
      menu_struct[id]['item'] = item
      if menu_struct[id].get('action'):
        EVT_MENU(frame, id, menu_struct[id]['action'])


  return menu
