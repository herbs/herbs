#!/usr/bin/env python
#Boa:App:HerbsApp

from wxPython.wx import *

from herbs.GUI import main_frame

modules ={'importRulesDialog': [0, '', 'importRulesDialog.py'],
 'main_frame': [1, 'Main frame of Application', 'main_frame.py'],
 'test_frame': [0, '', 'test_frame.py'],
 'test_frame_panel': [0, '', 'test_frame_panel.py']}

class HerbsApp(wxApp):
    def OnInit(self):
        wxInitAllImageHandlers()
        self.main = main_frame.create(None)
        # needed when running from Boa under Windows 9X
        self.SetTopWindow(self.main)
        self.main.Show();#self.main.Hide();self.main.Show()
        return True

def main():
    import sys
    if len(sys.argv)>1:
        filename = sys.argv[1]
    else:
        filename=None
    from herbs.app import conn
    conn.init(filename)
    application = HerbsApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
