# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Documents the interface for the progress bar optionally used by various
methods in HERBS

"""

class ProgressBarBase:
  """
  Defines the two following fields:
  
    - `minimum`: defaults to 0,
    - `maximum`: defaults to 100.

  Both of these fields should be set before `setValue()` is called for the
  first time.

  """
  minimum=0 ## TBD: unused for now!
  maximum=100
  
  def setValue(self, value):
    """
    Updates the current value for the progress bar.
    Subclasses must implement this method.

    :param value: the current value. It should normally be greater or equal to
      `self.minimum`, and lower or equal to `self.maximum`.

    """
    pass
