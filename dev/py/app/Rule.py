class Rule(dict):
  """
  This simple class holds a rule.

  A rule is said to be valid iff it defines at least the keys `head` and
  `tail`.  A third key `id` exists and usually refer to the corresponding
  primary key in the database.

  :see: herbs.app.rules_set.make_rule
  """
  def __init__(self, head, tail, id=None):
    self['head'] = head
    self['tail'] = tail
    self['id'] = id
    # this attribute is NOT saved in the database, only used in
    # compatible.N_best_rules_among_k() for the moment being
    self.count=0

  def __hash__(self):
    return hash(self['head']) ^ hash(self['tail'])

  def __eq__(self, rule):
    """
    Two rules are equal iff they are instance of ``dict`` and they have
    the same values assigned to the keys ``head`` and ``tail``.
    
    Implementation note: some code in herbs require that equality is defined
    that way (ex.: `compatible.N_best_rules_among_k`)
    """
    return isinstance(rule, dict) \
           and self['head']==rule['head'] \
           and self['tail']==rule['tail']
