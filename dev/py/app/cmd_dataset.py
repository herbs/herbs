# -*- coding: iso-8859-1 -*-
## LICENSE
"""

This modules defines the command ``dataset`` implemented by class
`cmd_dataset`.

"""
import getopt, sys
from herbs.utils import log
from herbs.app import dataset

class cmd_dataset:
  """

  Implements command ``dataset``.
  
  """
  available_subcommands={'import': ( 'hn:xs:l:t:',
                                     ["help", "name=",
                                      # csv related options
                                      "csv", "csv-separator=",
                                      "csv-list-attr-names=",
                                      "csv-list-attr-types="] ),
                         'delete': ( 'h',
                                     ["help", ] )
                         }
  subcommand = None
  filename = None
  name = None  # defaults to filename
  format = None
  # csv related options
  csv_sep = ','
  csv_attrs = None
  csv_attrs_types = None
  
  def parse(self,args):
    """

    :except ValueError: if ``len(args)<3`` (the command requires a subcommand
      and a file path, at least), or if there are unrecognized options.

    """
    log.debug("%s"%args)
    if not args:
      raise ValueError, "command dataset couldn't parse options"

    self.subcommand=args[1]
    if self.subcommand in ['-h','--help']:
      self.subcommand=None
      self.usage()
      return args[2:]

    if len(args[1:])<2:
      raise ValueError, "command dataset couldn't parse options"

    if self.subcommand not in self.available_subcommands:
      raise ValueError, "Command 'import' got an unknown subcommand %s",args[1]

    try:
      from getopt import getopt
      short,long=self.available_subcommands[self.subcommand]
      options,args = getopt(args[2:], short, long)
    except:
      self.usage()
      raise ValueError, "Unable to parse options. Aborting"

    csv = name = False
    for k, v in options:
      if k in ('-h', '--help'): self.usage(); continue
      # Options for 'import'
      if k in ('-n', '--name'): name = v;
      if k in ('-x', '--csv'): csv = True; continue
      if k in ('-s', '--csv-separator'): self.csv_sep = v; continue
      if k in ('-l', '--csv-list-attr-names'): self.csv_attrs = v; continue
      if k in ('-t', '--csv-list-attr-types'): self.csv_attrs_types=v; continue
      # Options for 'delete': None for now

    if len(args) < 1:
      self.usage()
      raise ValueError, "dataset: sub-command import requires a file path"

    if self.subcommand == 'import':
      self.filename = args[0]
      import os
      self.name = name or os.path.splitext(os.path.basename(self.filename))[0]
      self.name = self.name.replace('.', '_') # TBD FIX THIS

    elif self.subcommand == 'delete':
      self.name = args[0]
      
    # Handling options related to dataset/csv import
    if len(self.csv_sep) != 1:
      raise ValueError, "csv separator must be a single character"

    if self.csv_attrs:
      self.csv_attrs = self.csv_attrs.split(':')
    if self.csv_attrs_types:
      self.csv_attrs_types = self.csv_attrs_types.split(':')
    if self.csv_attrs and self.csv_attrs_types:
      if len(self.csv_attrs) != len(self.csv_attrs_types):
        raise ValueError, 'Supplied attributes names and types must have the same length!'
      
    # TBD: check that the specific options are supplied iff the corresponding
    # TBD: subcommand is set

    self.format = csv and 'csv' or None

    if self.subcommand=='import' and not self.format:
      raise ValueError, "Error: dataset import: unspecified format"
    return args[1:]
    
  def run(self):
    """
    """
    if not self.subcommand: # cf. --help after command 'dataset'
      return
    
    if self.subcommand=='import'  and self.format == 'csv':
      dataset.import_csv(self.filename, self.name, self.csv_sep,
                         self.csv_attrs, self.csv_attrs_types)

    elif self.subcommand == 'delete':
      # TBD if -Q ... (really quiet -> ignore exception and log)
      log.info('deleting dataset %s', self.name)
      dataset.delete_dataset(self.name)

    else:
      raise RuntimeError, "Unknown command %s"%self.subcommand

  def usage(self):
    sys.stderr.write(usage())


def usage():
  """
dataset <subcommand> ...

Subcommands
-----------

  import [options] <filename>    Parse a file
  delete <dataset_name>          Deletes a dataset from the HERBS database

Generic options
---------------
  -h --help    this help

Options for `import`
--------------------

  -x --csv       data in file are stored in csv format 
                 This option enables
                 
  -n --name      the name of the dataset. If it is not supplied, it
                 defaults to the filename (without the extension)

--csv specific options
  -s --csv-separator         the separator used in the csv. Defaults to ','

  -l --csv-list-attr-names   The list of the attributes' names, separated by
                             semi-colons, such as in: "attr1:attr2:attr3"

                             If it is not supplied, the attributes' names are
                             extracted from the first line of the file.

  -t --csv-list-attr-types   The list of the attributes' names, separated by
                             semi-colons.
                             Acceptable values are: 'float', 'int', 'text'
                             Example: "float:float:text"

                             If it is not supplied, then the attributes' types
                             will be guessed from the file contents. Warning:
                             this might take some time on big files.

  """
  return usage.__doc__
