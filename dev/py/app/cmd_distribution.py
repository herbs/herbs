# -*- coding: iso-8859-1 -*-
## LICENSE
"""

This modules defines the command ``distribution`` implemented by the class
`cmd_distribution`.

"""
import getopt, sys
from herbs.utils import log
from herbs.app import distribution

class cmd_distribution:
  """

  Implements command ``distribution``.
  
  """
  available_subcommands={'1': ( 'hr:d:x:y:m:XYLo:g',
                                [ "help", "rulesset=",
                                  "dataset=", "x-axis=", "y-axis=", "measure=",
                                  "semilogx", "semilogy", "loglog",
                                  "output-file=", "grid" ] ),
                         }
  rulesset = dataset = None
  x_axis = y_axis = None
  log_x = log_y = False
  grid = False
  
  def __init__(self):
    self.filename = []
  
  def parse(self,args):
    """

    :except ValueError: TBD

    """
    log.debug("%s"%args)
    if not args or len(args[1:])<1:
      raise ValueError, "command distribution couldn't parse options: missing arguments"

    self.subcommand=args[1]
    if self.subcommand in ['-h','--help']:
      self.subcommand=None
      self.usage()
      return args[2:]

    elif self.subcommand not in self.available_subcommands:
      raise ValueError,\
            "Command 'distribution' got an unknown subcommand: %s" % args[1]

    try:
      from getopt import getopt
      short,long=self.available_subcommands[self.subcommand]
      options,args = getopt(args[2:], short, long)
    except:
      self.usage()
      raise ValueError, "Unable to parse options. Aborting"

    for k, v in options:
      if k in ('-h', '--help'): self.usage(); continue
      # Options for '1'
      # TBD: replace w/ 
      if k in ('-r', '--rulesset'): self.rulesset = v
      if k in ('-d', '--dataset'): self.dataset = v
      if k in ('-x', '--x-axis'):  self.x_axis = v
      if k in ('-m', '--measure',
               '-y', '--y-axis'):   self.y_axis = v
      if k in ('-X', '--semilog-x'): self.log_x = True
      if k in ('-Y', '--semilog-y'): self.log_y = True
      if k in ('-L', '--loglog'): self.log_x = self.log_y = True
      if k in ('-o', '--output-file='): self.filename.append( v )
      if k in ('-g', '--grid'): self.grid = True
    # TBD check mandatory args.
    return args
    
  def run(self):
    """
    """
    if not self.subcommand: # cf. --help after command 'dataset'
      return
    if self.subcommand=='1':
      distribution.distribution(self.rulesset, self.dataset,
                                self.x_axis, self.y_axis,
                                self.log_x, self.log_y,
                                self.filename, self.grid)

    else:
      raise RuntimeError, "Unknown command %s"%self.subcommand

  def usage(self):
    sys.stderr.write(usage())


def usage():
  """
distribution <subcommand> [options]

Subcommands:
------------

'1' (!)

Options
-------
::

  -r  --rulesset <name>  name of a rules set
  -d  --dataset <name>   name of a dataset

  -x  --x-axis <name>    tells what should be displayed on the X axis. <name>
                         is either 'na', 'nb', 'nab' or 'nab_', or one of the
                         existing quality measures (see list -Q for a complete
                         listing)

  -y  --y-axis  <name>
  -m  --measure <name>   the quality measure whose value should be displayed
                         on the Y axis
  
  -X  --semilog-x
  -Y  --semilog-y
  -L  --loglog

Display options::

  -g  --grid             whether a grid should be displayed
  -o  --output-file <filename>

TBD
  """
  return usage.__doc__
