# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Initialization of a new database for HERBS
"""
import getopt
import logging
log = logging.getLogger('herbs')

class cmd_init:
  """

  Implements commands ``init`` and ``use``. The former creates a brand new
  database for HERBS, the latter simply connects HERBS to the supplied
  database.
  
  """
  dbname=None
  create=False
  
  def parse(self,args):
    """

    :except ValueError: if ``len(args)<2`` (the command needs a database
      name), or if there are unrecognized options.

    """
    log.trace("%s"%args)
    if not args[1:]:
      raise ValueError, "command init requires a database name"
    if args[0]=='init':
      self.create=True
    #import pdb ; pdb.set_trace()
    try:
      options, args = getopt.getopt(args[1:], 'h', ["help"])
    except:
      self.usage()
      raise ValueError, "Unable to parse options. Aborting"

    for k, v in options:
      if k in ('-h', '--help'): self.usage();

    if len(args)<1:
      self.usage()
      raise ValueError, "command init requires a database name"
    
    self.dbname=args[0]
    return args[1:]
    
  def run(self):
    """
    """
    from herbs.app import conn
    conn.init(self.dbname, self.create)

  def usage(self):
    usage()

def usage():
  """
  init <database filename>    Initializes a new database for HERBS
  use  <database filename>    Tell HERBS to use that database
  Option::

    -h --help    this help
  """
  return usage.__doc__
