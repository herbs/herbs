# -*- coding: iso-8859-1 -*-
## LICENSE
"""

This modules defines the command ``rulesset`` implemented by the class
`cmd_rulesset`.

"""
import getopt, sys
from herbs.utils import log
from herbs.app import dataset

class cmd_rulesset:
  """

  Implements command ``rulesset``.
  
  """
  available_subcommands={'import': ( 'hact:n:',
                                     ["help", "apriori",
                                      "c45", "c45-tail-attr-name=", "name="] ),
                         'delete': ( 'h',
                                     ["help", ] )
                         }
  filename = None
  name = None  # defaults to filename
  format = None    # either 'c45' or 'apriori', cf. parse()
  c45_attr = ()
  
  def parse(self,args):
    """

    :except ValueError: if ``len(args)<3`` (the command requires a subcommand
      and a file path, at least), or if there are unrecognized options.

    """
    log.debug("%s"%args)
    if not args or len(args[1:])<2:
      raise ValueError, "command rulesset couldn't parse options"

    self.subcommand=args[1]
    if self.subcommand in ['-h','--help']:
      self.subcommand=None
      self.usage()
      return args[2:]

    elif self.subcommand not in self.available_subcommands:
      raise ValueError,\
            "Command 'rulesset' got an unknown subcommand: %s" % args[1]

    try:
      from getopt import getopt
      short,long=self.available_subcommands[self.subcommand]
      options,args = getopt(args[2:], short, long)
    except:
      self.usage()
      raise ValueError, "Unable to parse options. Aborting"

    apriori = c45 = csv = name = False
    c45_attr = ()
    for k, v in options:
      if k in ('-h', '--help'): self.usage(); continue
      # Options for 'import'
      if k in ('-n', '--name'): name = v;
      if k in ('-a', '--apriori'): apriori = True; continue

      if k in ('-c', '--c45'): c45 = True; continue
      if k in ('-t', '--c45-tail-attr-name'): c45_attr = v;

    if len(args) < 1:
      self.usage()
      if self.subcommand == 'import':
        raise ValueError, "rulesset: sub-command import requires a file path"
      elif self.subcommand == 'delete':
        raise ValueError, "rulesset: sub-command delete requires a rulesset name"


    ## Handling options related to sub-command 'import'
    if self.subcommand == 'import':

      self.filename = args[0]
      import os
      self.name = name or os.path.splitext(os.path.basename(self.filename))[0]
      self.name = self.name.replace('.', '_') # TBD FIX THIS
      
      if (c45 and apriori) or (not c45 and not apriori):
        self.usage()
        raise ValueError, "Incorrect configuration of options"
    
      # TBD: check that the specific options are supplied iff the corresponding
      # TBD: command is set
      self.format = apriori and 'apriori' or c45 and 'c45' or None

      if not self.format:
        raise ValueError, "Error: rulesset import: unspecified format"

    ## Handling options related to sub-command 'delete'
    if self.subcommand == 'delete':
      self.name = args[0]

    # if this is not c45, then it should be a false value so that
    # discard_no_head gets its default value
    self.c45_attr = c45 and (c45_attr,) or ()
    return args[1:]
    
  def run(self):
    """
    """
    from herbs.app import rules_set

    if not self.subcommand: # cf. --help after command 'dataset'
      return
    if self.subcommand=='import':
      if self.format in ('apriori', 'c45'):
        rules_set.generic_rules_import(self.filename, self.format, self.name,
                                       *self.c45_attr)

    elif self.subcommand == 'delete':
      # TBD if -Q ... (really quiet -> ignore exception and log)
      log.info('deleting rules set %s', self.name)
      rules_set.delete_rules_set(self.name)

    else:
      raise RuntimeError, "Unknown command %s"%self.subcommand

  def usage(self):
    sys.stderr.write(usage())


def usage():
  """
rulesset <subcommand> [options] <filename>

Subcommands
-----------

  import               Parse a file
  delete               TBD

Generic options
---------------
  -h --help    this help

Options for `import`
--------------------
  The import subcommand requires that one and only one format is given
  -a --apriori   rules in file are stored in the "apriori" format
  -c --c45       rules in file are stored in the "c4.5" format

  -n --name      the name of the rules set. If it is not supplied, it
                 defaults to the filename (without the extension)

--c45 specific options
  -t --c45-tail-attr-name  description TBD

  """
  return usage.__doc__
