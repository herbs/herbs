#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
from herbs.utils import log

def is_compatible_with(rulesset_name, dataset_name):
  """

  Tells whether the rules set and the dataset are compatible one with the
  other. They are said to be compatible if and only if the set of attributes
  used in the rules set is a subset of the set of attributes used in the
  dataset. In other words, this means that every rule in the rule set can be
  evaluated on every point in the dataset.
  
  :Parameters:
    - `rulesset_name`: the rules set's name
    - `dataset_name`: the dataset's name (!)

  :exception ValueError: if no such dataset and/or rules set with the supplied
    names exist.
  """
  import dataset, rules_set
  ds_attributes=dataset.attributes_names(dataset_name)
  rs_attributes=rules_set.attributes(rulesset_name)
  return [attr for attr in rs_attributes if attr not in ds_attributes] == []
  

def compatible_couples():
  """

  Returns all compatible couples (dataset, rules set) declared in the
  current herbs database

  """
  compatible=[]
  from herbs.app import dataset, rules_set
  for rs in rules_set.rules_sets().keys():
    for ds in dataset.datasets().keys():
      if is_compatible_with(rs, ds):
        compatible.append([rs, ds])
  return compatible

def taux_couverture(rs, ds):
  """
  """
  from herbs.app import rules_set, dataset, conn
  rs_id=rules_set._id(rs)

  #sql= "SELECT COUNT(t0.id) FROM DS_%s t0 "\
  #     "WHERE EXISTS (SELECT t1.id FROM RULES t1"\
  #     "              WHERE t1.FK_RULES_SET=%li AND t2."
  #     (ds,rs_id,)
  sql = "SELECT ROWID FROM DS_%s t0 WHERE NOT %%(head)s OR NOT %%(tail)s"%ds

  non_covered_data_ids=None
  cur=conn.cursor()

  for rule in rules_set.rules_named(rs):
    cur.execute(sql%rule)
    if non_covered_data_ids is None:
      non_covered_data_ids = [id[0] for id in cur.fetchall()]
      continue

    if non_covered_data_ids == []:
      log.debug("no more uncovered rules, no need to check more rules")
      break
    
    ids = [id[0] for id in cur.fetchall()]
    non_covered_data_ids = [ id for id in ids  # intersection
                             if id in non_covered_data_ids ]

  uncovered_nbs = len(non_covered_data_ids)
  non_covered_data_ids = ids = None # discard

  # make this a float for the result to be a float
  total = float(dataset.nb_instances(ds)) 
  #print total, uncovered_nbs
  return (total - uncovered_nbs)/ total

def N_best_rules(rs, ds, quality_measure, N, rules_only=False):
  """
  Returns the list of the N rules that are best ranked when classified by a
  given quality measure. If `N` is ``None`` or greater than the total number
  of rules, the whole set of rules is returned.
  
  :Parameters:
    - `ds`: name of a dataset
    - `rs`: name of a rules set
    - `quality_measure`: name of a quality measure
    - `N`: the minimum number of rules the method will return. The number of
      returned rules is higher then ``N`` if and only if the Nth rule and the
      next ``n`` ones are placed equal: in this case, the method returns the
      ``N+n`` rules.  Special value ``None`` means: return every rules.
    - `rules_only`: see documentation for the returned value, below

  :return: if `rules_only` is ``False`` (the default), the method returns a
    list of 2-tuples made of a `Rule` and its associated value for the
    requested quality measure.  The returned list is sorted by the values in
    descending order (the best one is the first one in the list).

    If `rules_only` is ``False``, the returned list consists in rules only.
  
  :exception ValueError: if the quality measure has not been computed on
    rs:ds, or if no such rules set or dataset exist.
  """
  from herbs.app import rules_set, dataset, conn, quality_measures
  rs_id=rules_set._id(rs)
  ds_id=dataset._dataset_id(ds)
  quality_measure = quality_measure.upper()
  
  # check quality_measure
  if not quality_measures.is_computed(quality_measure, rs, ds):
    raise ValueError, "Measure %s is not evaluated for rs: %s, ds: %s"%(quality_measure,rs,ds)

  cur=conn.cursor()

  # Get the pivotal value
  pivotal_value=None
  if N is not None:
    sql = "SELECT value FROM (" \
          "  SELECT ifnull(t0.value,'+inf') as value FROM QUALITY_MEASURES t0 " \
          "  WHERE t0.FK_RULES_SET=%li AND t0.FK_DataSet=%li AND t0.NAME='%s'"\
          "  ORDER BY ifnull(t0.value,'+inf') DESC LIMIT %li) " \
          "ORDER BY value ASC LIMIT 1"%(rs_id, ds_id, quality_measure,N,)
    cur.execute(sql)
    pivotal_value=cur.fetchone()[0]
    if pivotal_value is None:
      import pdb ; pdb.set_trace()
  # and finally get the values:
  # Note: under SQLite '+inf'>0.3, but '+inf'<'0.3'...
  sql = "SELECT t1.id,t1.head,t1.tail,ifnull(t0.value,'+inf') as fixed_value "\
        "FROM QUALITY_MEASURES t0, RULE t1 "\
        "WHERE t0.FK_DataSet=%li "\
        "  AND t0.FK_RULES_SET=%li "\
        "  AND t0.NAME='%s' "\
        "  AND t1.id=t0.FK_Rule " % (ds_id, rs_id, quality_measure)
  if N is not None:
    sql+= " AND fixed_value>=%r "%pivotal_value

  sql+="ORDER BY fixed_value DESC"
  log.trace(sql)
  cur.execute(sql)
  if not rules_only:
    return [(rules_set.make_rule(r[1], r[2], id=r[0]), r[3])
            for r in cur.fetchall()]
  else:
    return [rules_set.make_rule(r[1], r[2], id=r[0]) for r in cur.fetchall()]
  
def N_best_rules_among_k(rs, ds, quality_measures, N, k):
  """
  k<len(quality_measures)
  """
  if k>len(quality_measures):
    raise ValueError, "k should be lower or equal to N"
  import sets
  
  ## k==1 or k==len(len(quality_measures)) are special cases:
  #if k in (1, len(quality_measures)):
  #  _set = sets.Set()
  #  if k=1:
  #    op=_set.union_update
  #  else:
  #    op=_set.intersection_update
  #
  #  for m in quality_measures:
  #    op(N_best_rules(rs, ds, quality_measures, N, rules_only=True))
  #
  #  return list(_set)

  # else, count!
  best = {} # 'quality_measure': [N_best_rules]
  union_set = sets.Set()
  for measure in quality_measures:
    _n_best = N_best_rules(rs, ds, measure, N, rules_only=True)
    best[measure]=_n_best
    union_set.union_update(_n_best)

  for rule in union_set:
    for measure, b in best.items():
      if rule in b:
        rule.count += 1
        rule.setdefault('selected_by', []).append(measure)

  ret = [(r,r.count) for r in union_set if r.count>=k]
  ret.sort( lambda r1, r2: cmp(r2[1], r1[1]) )
  return ret
