import sqlite3

from herbs.utils import log

cnx=_cur=None
current_dbname=None

def init(dbname, create=False):
  """Initializes the module by opening the db stored in file ``dbname``.

  :Parameters:
    - `dbname`: (relative or absolute) path of the database. If None, the
      previously opened database is closed.

    - `create`: if false (default), no database is created, and the method
      raises IOError if `dbname` does not point to a valid sqlite db-file.
  """
  global cnx, _cur, current_dbname

  if dbname == None:
    log.debug('Closing database %s'%current_dbname)
    current_dbname = _cur = cnx = None
    return
  
  if not create and dbname == current_dbname:
    return

  import os
  log.debug('Opening database %s'%dbname)

  if not create and not os.path.exists(dbname):
    raise IOError, "Cannot use database '%s': file does not exist"%dbname

  if create and os.path.exists(dbname):
    raise IOError,"Cannot initialize database '%s': file already exists"%dbname

  cnx = sqlite3.connect(dbname)
  current_dbname = dbname
  if create:
    # commit the db schema
    from herbs.app import sqlite_defs
    for statement in sqlite_defs.db_schema:
      #import pdb; pdb.set_trace()
      cnx.cursor().execute(statement)
    cnx.commit()
  _cur = cnx.cursor()

def isConnected():
  "Tells whether the module is connected to a database"
  return current_dbname is None

def cursor():
  return cnx.cursor()

def commit():
  cnx.commit()
  
def rollback():
  cnx.rollback()

def execute(sql_statement):
  _cur.execute(sql_statement)
