#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""

This module is dedicated to the computation of the so-called evaluations,
which are the figures from which almost all quality measures are derived.

Concretely, those evaluations consist in four numbers calculated for a
dataset and a rule. Say that a given rule is divided into

: ``na``, ``nb``, ``nab`` and ``nab_``
- ``na``: 
- ``nb``: 
- ``nab``: 
- ``nab_``: 
"""

from herbs.app import conn, dataset, rules_set, compatible
from herbs.utils import log

def evaluate(rulesset_name, dataset_name, progressBar=None):
  """

  Computes and caches in the HERBS db the evaluations for the given rules set
  and dataset.

  :Parameters:
    - `rulesset_name`: name of the rules set
    - `dataset_name`: name of the dataset
    - `progressBar`: (optional) if supplied, 
    
  :exception ValueError: if no such dataset or rulesset with the supplied name
    exist
  """
  log.info('Computing evaluations for rules set: %s and dataset: %s',
           rulesset_name, dataset_name)
  if is_evaluated(rulesset_name, dataset_name):
    return
  if not compatible.is_compatible_with(rulesset_name, dataset_name):
    raise ValueError, 'Incompatible rules set and dataset'
  
  ds_id=dataset._dataset_id(dataset_name)
  rs_id=rules_set._id(rulesset_name)

  cur=conn.cursor()
  sql="INSERT INTO "                                                    \
       "EVALUATIONS(FK_DataSet,FK_RULES_SET,FK_Rule,na,nb,nab,nab_)"    \
       "VALUES "                                                        \
       "( %li,%li,%%(id)li,"                                            \
       "  (SELECT COUNT(*) FROM DS_%s WHERE %%(head)s), "               \
       "  (SELECT COUNT(*) FROM DS_%s WHERE %%(tail)s), "               \
       "  (SELECT COUNT(*) FROM DS_%s WHERE %%(head)s AND %%(tail)s), " \
       "  (SELECT COUNT(*) FROM DS_%s WHERE %%(head)s AND NOT %%(tail)s) "  \
       ")"%((ds_id,rs_id)+(dataset_name,)*4)
  try:
    idx = 0
    if progressBar:
      progressBar.minimum=0
      progressBar.maximum=rules_set.nb_rules(rulesset_name)
    for rule in rules_set.rules_named(rulesset_name):
      _sql=sql%rule
      log.trace("evaluating rule %s:%i on %s w/ SQL: %s",
                rulesset_name, rule['id'], dataset_name, _sql)
      cur.execute(_sql)
      if progressBar:
        idx += 1
        if not progressBar.setValue(idx):
          conn.rollback()
          return
  except:
    conn.rollback()
    raise
  else:
    conn.commit()

def is_evaluated(rulesset_name, dataset_name):
  """

  :exception ValueError: if either the dataset or the rules set do not exist
  """
  # check that we have an entry for the dataset and a rule in the rules set
  ds_id=dataset._dataset_id(dataset_name)
  a_rule_id=rules_set._rules_id_named(rulesset_name, limit=1)[0]
  cur=conn.cursor()
  try:
    cur.execute('SELECT COUNT(*) FROM EVALUATIONS WHERE FK_DataSet=%li and FK_Rule=%li'%(ds_id, a_rule_id))
    return cur.fetchone()[0]!=0
  except:
    conn.rollback()
    raise
  
def available_evaluations():
  """
  Returns all couple (rules set, dataset) for which the evaluations has already
  been computed

  :return: a list of couples, each made of a rules set's name and a dataset's
    name.
  """
  cur=conn.cursor()
  sql="SELECT DISTINCT EVALUATIONS.FK_DataSet, RULES_SET.name "\
       "FROM EVALUATIONS, RULES_SET, RULE "\
       "WHERE RULES_SET.id=RULE.FK_RULES_SET AND RULE.id=EVALUATIONS.FK_Rule"
  try:
    cur.execute(sql)
    fkds_rsname=cur.fetchall()
  except:
    conn.rollback()
    raise

  res=[]
  for ds_id,rs_name in fkds_rsname:
    ds=dataset._dataset_name_for_id(ds_id)
    res.append((rs_name, ds))

  return res
  
def evals(rulesset_name, dataset_name):
  """
  :return: a list of list containing 'na', 'nb', nab, nab_, head and tail
  :exception ValueError: if either the dataset or the rules set do not exist
  """
  # check that we have an entry for the dataset and a rule in the rules set
  ds_id = dataset._dataset_id(dataset_name)
  rs_id = rules_set._id(rulesset_name)

  cur=conn.cursor()
  try:
    cur.execute('SELECT DISTINCT t0.na, t0.nb, t0.nab, t0.nab_, t1.head, t1.tail FROM EVALUATIONS t0, RULE t1, RULES_SET WHERE t0.FK_DataSet=%li and t0.FK_Rule=t1.id and t1.FK_RULES_SET=%li ORDER BY t1.ID'%(ds_id, rs_id))
    return cur.fetchall()
  except:
    conn.rollback()
    raise
  

def evaluation(rule_id, dataset_id):
  """
  Returns the evaluations computed for the given rule on the given dataset

  :param rule_id: an integer
  :param dataset_id: an integer

  :return: a dictionary mapping ``'na'``, ``'nb'``, ``'nab'`` and ``'nab_'``
    to their corresponding values.
  """
  cur=conn.cursor()
  _sql = 'SELECT na, nb, nab, nab_ FROM EVALUATIONS ' \
         'WHERE FK_RULE=%li AND FK_DATASET=%li'%(rule_id,dataset_id)
  cur.execute(_sql)
  assert cur.rowcount==1, "Got %li row(s) instead of: 1 for sql: %s"%(cur.rowcount, _sql)
  return dict(cur.fetchone())
