## LICENSE
"""
This modules holds all quality measures that can be used within HERBS

It defines:

  - `measures`, a dictionary mapping quality measures to their associated
    values,

  - `measures_func`, ...

:group Computing evaluations: evaluate, evaluate_all, evaluate_IPD
:group Getting informations: computed_measures, get_measures, has_computed_measures, is_computed
:group internals: _evaluate
:group Defined measures: SUP, CONF, R, CONFCEN, PS, LOE, ZHANG, INDIMP, LIFT,
  MOCO, SEB, MC, CONV, TEC, IQC, GI, LAP, II, EII

"""
SUP="nab/N" # ( na - (na-nab) ) / N

CONF="nab/na" # 1 - (na-nab)/na

R="(N*nab-na*nb)/sqrt(N*na*nb*(N-na)*(N-nb))"

CONFCEN="(N*nab-na*nb)/(N*na)"

PS="( na*(N-nb)/N - (nab_) )/N"

LOE="1-(N*(na-nab)/na*(N-nb))"

ZHANG="(N*nab-na*nb)/max(nab*(N-nb), nb*nab_)"

#INDIMP="- ( N*(na-nab)-(na*(N-nb)) ) / sqrt(N*na*(N-nb))"
INDIMP="( N*nab-na*nb ) / sqrt(N*na*(N-nb))"

LIFT="N*nab / ( na * nb )"

MOCO="(nab-nab_) / nb"

SEB="( na - nab_ ) / nab_"

MC="nab*(N-nb)/(nb*nab_)" # (na - (na-nab))*(N-nb)/(nb*(na-nab))

CONV="na*(N-nb)/(N*nab_)"

# TEC was previously declared as:
#   TEC="1 - 1 / ( na/nab_ - 1 )"
# Pb.: TEC can reach its maximum value == int(1) when nab_=0
#      but this formula is evaluated to NULL by sqlite: a division by zero
#      is evaluated as NULL and then all numerical functions applied to NULL
#      give NULL as their results...
# This is solved by declaring TEC differently:
TEC="( nab - nab_ ) / nab"

IQC="2 * (N*na - N*nab_ - na*nb) / (N*na+N*nb-2*na*nb)"

GI="log(N*nab)-log(na*nb)" # better 'log-log' than 'log(/)'

LAP="(nab+1)/(na+2)"

II="imp_idx(N,na,nb,nab)"

EII="eimp_idx(N,na,nb,nab)"

IPD=" see evaluate_IPD() "

import inspect
measures={}
[measures.setdefault(n,globals()[n])
 for n in globals().keys() if n.isalpha() and n==n.upper()]
measures_func={}
[measures_func.setdefault(name, lambda N,na,nb,nab,nab_: eval(expr))
 for name, expr in measures.items()]
del inspect, n, name, expr

from herbs.utils import log

def _evaluate(name, measure_expr, rulesset_id, rule_id, dataset_id, db_cursor):
  """
  
  Computes and caches in the HERBS db the quality measures `name` on the given
  rule and dataset. This is mostly for internal use, users usually need to
  evaluate a quality measures for a whole rules set and dataset instead: see
  `evaluate()`.

  **DO NOT USE THIS METHOD TO COMPUTE ``IPD``: see `evaluate_IPD()`**

  :Parameters:
    - `name`: name of an existing measure (NOT CHECKED)
    - `measure_expr`: 
    - `rulesset_id`: identifier for the rule set in the HERBS db
    - `rule_id`: identifier for the rule in the HERBS db
    - `dataset_id`: name of the dataset
    - `db_cursor`: 

  :see: `evaluate`

  :exception ValueError: if no such measure `name` exists
  
  """
  sql="INSERT INTO QUALITY_MEASURES (id, name, FK_RULES_SET, FK_Rule, FK_Dataset, value) VALUES (NULL,\"%s\",%li,%li,%li,(SELECT %s FROM EVALUATIONS WHERE FK_DataSet=%li AND FK_Rule=%li))"%(name, rulesset_id, rule_id, dataset_id, measure_expr, dataset_id, rule_id)
  log.trace("_evaluate: %s", sql)
  db_cursor.execute(sql)

def evaluate(name, rulesset_name, dataset_name, handle_txn=True,
             progressBar=None, nested_idx=None):
  """
  Evaluate the given measure on the couple (rs, ds)

  :Parameters:
    - `name`: name of the measure to evaluate
    - `rulesset_name`: name of a rules set
    - `dataset_name`: name of a dataset
    - `handle_txn`: (default: True) if True, the method commit() or rollback()
      the underlying database connection upon success or failure. If False, the
      transaction is left untouched and it is the caller's responsability to
      handle it.
    - `progressBar`: (optional) if supplied.  See `herbs.app.IProgressBar`.

  :return: False if the progressBar is set and if the user used it to cancel
    the task (in this case, the txn is rolled back unless `handle_txn` is
    ``False``). Returns True otherwise, indicating success.
  
  :exception ValueError: if no such rulesset, dataset or measure with the
    supplied name exist
 """
  name = name.upper()
  
  from herbs.app import dataset, rules_set, evaluations, compatible

  if is_computed(name, rulesset_name, dataset_name):
    if progressBar:
     if nested_idx:
       max_reached = rules_set.nb_rules(rulesset_name) + nested_idx
     else:
       max_reached = progressBar.maximum
     if not progressBar.setValue(max_reached):
          handle_txn and conn.rollback()
          return False
    return True

  if not compatible.is_compatible_with(rulesset_name, dataset_name):
    raise ValueError, 'Incompatible rules set and dataset'
    
  if not evaluations.is_evaluated(rulesset_name, dataset_name):
    evaluations.evaluate(rulesset_name, dataset_name)
    
  log.info('evaluating quality measure %s on dataset:%s rulesset:%s',
           name, rulesset_name, dataset_name)
  measure=measures.get(name)
  if not measure:
    raise ValueError, 'Unknown measure: %s'%name

  if name == 'IPD':
    return evaluate_IPD(rulesset_name, dataset_name, handle_txn, progressBar,
                        nested_idx)
  
  ds_id = dataset._dataset_id(dataset_name)
  rs_id = rules_set._id(rulesset_name)
  measure=measure.replace('N', str(dataset.nb_instances(dataset_name)))

  import conn
  cur=conn.cursor()

  try:
    idx = 0
    if nested_idx is not None:
      idx = nested_idx
    if progressBar and nested_idx is None:
      progressBar.minimum=0
      progressBar.maximum=rules_set.nb_rules(rulesset_name)
    for rule_id in rules_set._rules_id_named(rulesset_name):
      _evaluate(name, measure, rs_id, rule_id, ds_id, cur)
      if progressBar:
        idx += 1
        if not progressBar.setValue(idx):
          handle_txn and conn.rollback()
          return False

  except:
    handle_txn and conn.rollback()
    raise
  else:
    handle_txn and conn.commit()
  return True


def evaluate_IPD(rulesset_name, dataset_name, handle_txn=True,
                 progressBar=None, nested_idx=None):
  """
  Evaluate the quality measure  ``IPD`` (Indice probabiliste discriminant)
  for the given rule set and dataset

    :Parameters:
      - `rulesset_name`: name of a rules set
      - `dataset_name`: name of a dataset
      - `handle_txn`, `progressBar`, `nested_idx`: (optional) see `evaluate()`

  :return: see `evaluate`
  
  :exception ValueError: if no such rulesset or dataset with the supplied name
    exist
"""
  # 1. be sure to compute INDIMP
  if not is_computed('INDIMP', rulesset_name, dataset_name):
    evaluate('INDIMP', rulesset_name, dataset_name, handle_txn=False)
  
  # 2. compute the mean and the standard deviation (sigma) of INDIMP
  from herbs.app import conn, rules_set, dataset

  cur = conn.cursor()
  rs_id = rules_set._id(rulesset_name)
  ds_id = dataset._dataset_id(dataset_name)
  d = { 'N': dataset.nb_instances(dataset_name),
        'rs_id': rs_id,
        'ds_id': ds_id,
        }
  _mean = "SELECT SUM(t0.value)/%(N)li FROM QUALITY_MEASURES t0 "\
          "WHERE t0.FK_DataSet=%(ds_id)li AND t0.FK_RULES_SET=%(rs_id)li "\
          "  AND name='INDIMP'" % d

  cur.execute(_mean)
  d['mean'] = cur.fetchone()[0]

  _sigma = "SELECT SQRT( SUM(SQR(t0.value-%(mean)r))/count(t0.value) ) "\
           "FROM QUALITY_MEASURES t0 "\
           "WHERE t0.FK_DataSet=%(ds_id)li and t0.FK_RULES_SET=%(rs_id)li "\
           "  AND name='INDIMP'"%d
  cur = conn.cursor()
  cur.execute(_sigma)
  d['sigma'] = cur.fetchone()[0]

  # 3. Compute IPD(rule) = 1-phi( (IndImp(rule) - mean)/sigma )
  cur=conn.cursor()

  d['IndImp'] = 'SELECT value FROM QUALITY_MEASURES '\
                'WHERE FK_DataSet=%(ds_id)li AND FK_Rule=%%(rule_id)li '\
                '  AND name="INDIMP"'%d
  measure_expr = "1-phi( ( (%(IndImp)s) - %(mean)r)/%(sigma)r )"%d
  try:
    idx = 0
    if nested_idx is not None:
      idx = nested_idx
    if progressBar and nested_idx is None:
      progressBar.minimum = 0
      progressBar.maximum = rules_set.nb_rules(rulesset_name)
    _sql = "INSERT INTO QUALITY_MEASURES " \
         "  (id, name, FK_RULES_SET, FK_Rule, FK_Dataset, value) " \
         "VALUES (NULL,\"IPD\",%li,%%(rule_id)li,%li, (SELECT %s)) "\
         %(rs_id, ds_id, measure_expr)
    for rule_id in rules_set._rules_id_named(rulesset_name):
      sql = _sql % {'rule_id': rule_id}
      log.trace("_evaluate: %s", sql)
      cur.execute(sql)

      if progressBar:
        idx += 1
        if not progressBar.setValue(idx):
          handle_txn and conn.rollback()
          return False

  except:
    handle_txn and conn.rollback()
    raise
  else:
    handle_txn and conn.commit()
  return True


def evaluate_all(rulesset_name, dataset_name, handle_txn=True,
                 progressBar=None): #, nested_idx=None):
  """
  Evaluates all quality measures for 
  """
  from herbs.app import dataset, rules_set, evaluations, conn
  max = iter_step = 0
  if progressBar:
    progressBar.minimum=0
    iter_step = rules_set.nb_rules(rulesset_name)
    max = progressBar.maximum = iter_step * len(measures)


  try:
    idx=0
    for measure in measures.keys():
      ret = evaluate(measure, rulesset_name, dataset_name, handle_txn=False,
                     progressBar=progressBar, nested_idx=idx)
      idx += iter_step
      if not ret:
        handle_txn and conn.rollback()
        return ret
  except:
    handle_txn and conn.rollback()
    raise
  else:
    handle_txn and conn.commit()
  return True


def is_computed(name, rulesset_name, dataset_name):
  """
  Tells whether the quality measure `name` has already been computed for the
  given rules set and dataset. 

  :Parameters:
    - `name`: name of the quality measure
    - `rulesset_name`: name of a rules set
    - `dataset_name`: name of a dataset

  :exception ValueError: if no such dataset or rules set exists. Note that the
    quality measure's `name` is not checked at all.
  
  """
  from herbs.app import conn, dataset, rules_set
  cur=conn.cursor()
  dataset_id=dataset._dataset_id(dataset_name)
  rulesset_id=rules_set._id(rulesset_name)
  name=name.upper()
  sql="SELECT COUNT(*) FROM QUALITY_MEASURES t0 "\
       "WHERE t0.name = '%s' and t0.FK_Dataset=%li "\
       "  AND t0.FK_RULES_SET=%li"%(name,dataset_id,rulesset_id)
  log.trace("is_computed: evaluating: %s", sql)
  cur.execute(sql)
  return cur.fetchone()[0]

def computed_measures(rs=None, ds=None):
  """
  Returns the list of computed measures, along with the corresponding dataset
  and rules set; if `rs` and `ds` are set, returns the list of measures
  already computed for the supplied rules

  Please note: if you just want to determine whether at least one measure has
  already been computed, you should consider calling `has_computed_measures()`
  instead since that method is esp. designed to answer (really) much
  quicker.

  :param rs: a rules set's name :param ds: a dataset's name

  :return:

    - if ``(ds,rs)==(None,None)``, returns a list made of: ``(qm_name,
      rs_name, ds_name)``, where ``qm_name`` is the name of a quality measure
      whose value already computed for rules set ``rs_name`` and dataset
      ``ds_name``

    - otherwise, simply returns the list of quality measures that are already
      computed for rules set `rs` and dataset `ds`.

  :except ValueError: if either `rs` or `ds` is set while the other is
    ``None``, or they do not correspond to existing rule sets/datasets.
  """
  from herbs.app import conn, dataset, rules_set
  cur=conn.cursor()
  if (rs, ds) == (None, None):
    sql = "SELECT DISTINCT t0.name, t2.name, t3.name "\
          "FROM QUALITY_MEASURES t0, RULES_SET t2, DATASET t3 "\
          "WHERE t0.FK_Dataset=t3.id "\
          "  AND t0.FK_RULES_SET=t2.id"
  else:
    if rs is None or ds is None:
      raise ValueError, "One of params rs & ds is None while the other is not"

    ds_id = dataset._dataset_id(ds)
    rs_id = rules_set._id(rs)
    
    sql = "SELECT DISTINCT t0.name "\
          "FROM QUALITY_MEASURES t0 "\
          "WHERE t0.FK_Dataset=%i "\
          "  AND t0.FK_RULES_SET=%i" % (ds_id, rs_id)
  log.trace("computed_measures: evaluating: %s", sql)
  cur.execute(sql)
  measures_list = cur.fetchall()

  if rs is not None:
    measures_list = [m[0] for m in measures_list]
    measures_list.sort()

  return measures_list

def has_computed_measures(rs, ds):
  """
  Tells whether the corresponding dataset and rules has some already computed
  measures.

  :param rs: a valid rules set's name
  :param ds: a valid dataset's name

  :return: boolean value

  :except ValueError: if `rs` or `ds` do not correspond to existing
    rule sets/datasets.

  :see: `computed_measures()`
  """
  from herbs.app import conn, dataset, rules_set
  cur=conn.cursor()
  if rs is None or ds is None:
    raise ValueError, "Parameter cannot be null"

  ds_id = dataset._dataset_id(ds)
  rs_id = rules_set._id(rs)
    
  sql = "SELECT t0.name "\
        "FROM QUALITY_MEASURES t0 "\
        "WHERE t0.FK_Dataset=%i "\
        "  AND t0.FK_RULES_SET=%i LIMIT 1" % (ds_id, rs_id)
  # LIMIT 1: a single row returned one is sufficient to conclude
  log.trace("has_computed_measures; evaluating: %s", sql)
  cur.execute(sql)
  has_measures = cur.fetchall()
  return not not has_measures

def get_measures(quality_measure, rulesset_name, dataset_name):
  """
  Returns the values of a quality measure computed on a compatible pair of
  rules set and dataset.  This is strictly equivalent to::

      from herbs.app.compatible import N_best_rules
      return N_best_rules(rulesset_name, dataset_name, quality_measure, None)

  Please refer to `compatible.N_best_rules` for full details.

  :Parameters:
    - `quality_measure`: name of a quality measure
    - `dataset_name`: name of a dataset
    - `rulesset_name`: name of a rules set

  :return: a list of 2-tuples made of a rule and its associated value, sorted
    by values (descending order).

  :exception ValueError: if the quality measure has not been computed on
    rulesset:dataset, or if no such rules set or dataset exist.
  """
  from herbs.app import compatible
  return compatible.N_best_rules(rulesset_name, dataset_name,
                                 quality_measure.upper(), None)
