#!/usr/bin/env python
## Licence
"""Run all tests."""
__version__='$Revision: $'[11:-2]
import sys, getopt
import utils
import unittest

if __name__ == "__main__":
  utils.fixpath()

import test_conn
import test_rules_set
import test_dataset
import test_compatibility
import test_evaluations
import test_quality_measures

def usage(prgName, exitStatus=None):
  _usage="""%s [-v] [-V]
Runs the tests for package herbs.app

Options
--------
  -v  Minimally verbose
  -V  Really verbose
  -p  profile
  -h  Prints this message
""" % prgName
  if exitStatus is not None:
    print _usage
    sys.exit(exitStatus)
  else:
    return _usage

def test_suite():
  suite=unittest.TestSuite()
  suite.addTest(test_conn.test_suite())
  suite.addTest(test_rules_set.test_suite())
  suite.addTest(test_dataset.test_suite())
  suite.addTest(test_compatibility.test_suite())
  suite.addTest(test_evaluations.test_suite())
  suite.addTest(test_quality_measures.test_suite())
  return suite

verbose=0 # we need this to be global, for use by profile.run()
def main(args):
  me=args[0]
  try: options, args = getopt.getopt(sys.argv[1:], 'hvVp')
  except: usage(me, 1)
  global verbose
  profile=0
  for k, v in options:
    if k=='-h': usage(me, 1)
    if k=='-v': verbose=1; continue
    if k=='-V': verbose="Y"; continue
    if k=='-p': profile=1; continue
  if args: usage(me, 1)

  if profile:
    import profile
    profile.run('utils.run_suite(test_suite(), verbosity=verbose)',
                'profile.out')
    return 
  else:
      return utils.run_suite(test_suite(), verbosity=verbose)

if __name__ == "__main__":
  errs = main(sys.argv)
  sys.exit(errs and 1 or 0)
