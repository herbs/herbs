#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Tests for module compatible

"""
import unittest
if __name__ == "__main__":
  import utils, sys
  utils.fixpath()

from herbs.app import compatible, rules_set, dataset
from herbs.app import conn
from herbs.utils import log, FileFormatError

class TestCompatibility(unittest.TestCase):
  """
  Tests for module compatible
  """
  def setUp(self):
    conn.init('test.db', create=True)
    self.cur=conn.cursor()

  def tearDown(self):
    del self.cur
    import os
    os.unlink('test.db')
    
  def test_01_is_compatible_with(self):
    "[compatible] is_compatible_with"
    self.assertRaises(ValueError, compatible.is_compatible_with,
                      'RS_does_not_exist','DS_does_not_exist')

    attrs=['buying','maint','doors','persons','lug_boot','safety','offer']

    # load DS car
    dataset.import_csv('data/car.data', 'car', ',', attrs)

    # attributes in the next RS are exactly the same than dataset car's
    rules_set.import_c45('data/car.c45', 'test_11a', 'offer')
    self.failUnless(compatible.is_compatible_with('test_11a','car'))
    
    # attributes in the next RS == strict subset for DS car 's attributes
    rules_set.import_apriori('data/rules.apriori', 'test_11b')
    self.failUnless(compatible.is_compatible_with('test_11b','car'))

    # this one is definitely not compatible
    rules_set.import_c45('data/car.c45', 'test_11c', 'attr_not_in_DS')
    self.failIf(compatible.is_compatible_with('test_11c','car'))

  def test_02_compatible_couples(self):
    "[compatible] compatible_couples"
    self.failIf(compatible.compatible_couples())

    dataset.import_csv('data/abalone_with_headers.csv','abalone',',')
    attrs=['buying','maint','doors','persons','lug_boot','safety','offer']
    dataset.import_csv('data/car.data', 'car', ',', attrs)
    rules_set.import_c45('data/car.c45', 'car_c45', 'offer')
    rules_set.import_apriori('data/rules.apriori', 'car_apriori')
    rules_set.import_c45('data/abalone.shrinked.c45', 'abalone_c45', 'Rings')
    rules_set.import_c45('data/abalone.shrinked.c45','abalone_c45_nop','does_not_exist')

    couples=compatible.compatible_couples()
    self.assertEqual(3, len(couples))
    self.failUnless(['car_c45', 'car'] in couples)
    self.failUnless(['car_apriori', 'car'] in couples)
    self.failUnless(['abalone_c45', 'abalone'] in couples)

  def test_03_couverture(self):
    "[compatible] taux couverture"
    attrs=['buying','maint','doors','persons','lug_boot','safety','offer']
    dataset.import_csv('data/car.data', 'car', ',', attrs)
    rules_set.import_c45('data/car.c45', 'car_c45', 'offer')
    self.assertEqual(1.0, compatible.taux_couverture('car_c45', 'car'))

  def test_04_N_best_rules(self):
    "[compatible] N_best_rules"
    rules_set.import_c45('data/car.c45', 'car_c45', 'offer')
    attrs=['buying','maint','doors','persons','lug_boot','safety','offer']
    dataset.import_csv('data/car.data', 'car', ',', attrs)
    from herbs.app import quality_measures
    quality_measures.evaluate('PS','car_c45','car')
    
    self.assertRaises(ValueError,
                      compatible.N_best_rules, 'BAD', 'car', 'PS', 3)
    self.assertRaises(ValueError,
                      compatible.N_best_rules, 'car_c45', 'car', 'CONF', 3) 

    PS_best_7 = compatible.N_best_rules('car_c45', 'car', 'PS', 7)
    self.assertEqual(13, len(PS_best_7))
    for idx in range(len(PS_best_7)-1):
      self.failUnless(PS_best_7[idx][1] >= PS_best_7[idx+1][1])
    
    # N_best_rules() should be case-insensitive wrt the measure's name
    PS_best_7 = compatible.N_best_rules('car_c45', 'car', 'ps', 7)
    self.assertEqual(13, len(PS_best_7))

  def test_05_N_best_rules_among_k(self):
    "[compatible] N_best_rules_among_k"
    rules_set.import_c45('data/car.c45', 'car_c45', 'offer')
    attrs=['buying','maint','doors','persons','lug_boot','safety','offer']
    dataset.import_csv('data/car.data', 'car', ',', attrs)
    from herbs.app import quality_measures
    quality_measures.evaluate('PS','car_c45','car')
    quality_measures.evaluate('CONF','car_c45','car')
    quality_measures.evaluate('CONV','car_c45','car')
    
    self.assertRaises(ValueError,
                      compatible.N_best_rules_among_k, ('BAD',),'car','PS',3,1)
    self.assertRaises(ValueError,
                      compatible.N_best_rules_among_k,
                      'car_c45', 'car', ('ZHANG',), 3, 1) 
    self.assertRaises(ValueError,
                      compatible.N_best_rules_among_k,
                      'car_c45', 'car', ('CONF',), N=3, k=6)

    # Both calls must return the very same set
    self.assertEqual(compatible.N_best_rules_among_k('car_c45', 'car',
                                                     ('PS','CONV'), N=2, k=2),
                     compatible.N_best_rules_among_k('car_c45', 'car',
                                                     ('CONV','PS'), N=2, k=2))
    
    # 'PS' and 'CONV' have 3 rules in common for N=2:
    # the 3 rules returned for 'PS'
    set=compatible.N_best_rules_among_k('car_c45', 'car', ('PS','CONV'),
                                        N=2, k=2)
    self.assertEqual(3, len(set))

    _ps=compatible.N_best_rules('car_c45', 'car', 'PS', 2)
    _conv=compatible.N_best_rules('car_c45', 'car', 'CONV', 2)

    set=compatible.N_best_rules_among_k('car_c45', 'car', ('PS','CONV'),
                                        N=2, k=1)
    self.assertEqual(len(_ps)+len(_conv)-3, len(set))

    # N_best_rules() should be case-insensitive wrt the measure's name
    set=compatible.N_best_rules_among_k('car_c45', 'car', ('ps','cOnv'),
                                        N=2, k=1)
    self.assertEqual(len(_ps)+len(_conv)-3, len(set))

def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestCompatibility, "test_"))
    return suite


if __name__ == "__main__":
  if '-v'  in sys.argv:
    import logging
    log.setLevel(logging.NOTSET)
  errs = utils.run_suite(test_suite())
  sys.exit(errs and 1 or 0)
