#! /usr/bin/env python
## LICENSE
"""
Tests for herbs.app.conn

"""
import unittest
if __name__ == "__main__":
  import utils, sys
  utils.fixpath()

from herbs.app import conn

class TestConn(unittest.TestCase):
  """
  Tests for module conn
  """
  def tearDown(self):
    import os
    try: os.unlink('test.db')
    except: pass
    
  def test_01_initial_state(self):
    "[conn] initial state"
    self.assertEqual(None, conn.cnx)
    self.assertRaises(AttributeError, conn.cursor)

  def test_02_init(self):
    "[conn] init"
    self.assertRaises(IOError, conn.init, 'this_does_not_exists.db')
    conn.init('test.db', create=True)
    import os
    self.failUnless(os.path.isfile('test.db'))
    cur=conn.cursor()
    # we won't check the whole schema, just that it has been initialized
    cur.execute('select count(*) from RULES_SET') # should not raise
    self.assertEquals(0, cur.fetchone()[0])
    
def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestConn, "test_"))
    return suite

if __name__ == "__main__":
    errs = utils.run_suite(test_suite())
    sys.exit(errs and 1 or 0)
