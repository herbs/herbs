#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Tests for quelity_measures

Requires: rules_set, dataset, evaluations

"""
import unittest
if __name__ == "__main__":
  import utils, sys
  utils.fixpath()

from herbs.app import conn, rules_set, dataset, evaluations
from herbs.app import quality_measures

class Test_Quality_Measures(unittest.TestCase):
  """
  Tests for module quality_measures
  """
  def setUp(self):
    conn.init('test.db', create=True)
    self.cur=conn.cursor()
    dataset.import_csv( 'data/car.data', 'ds_car', ',',
                        ( 'buying','maint','doors','persons','lug_boot',
                              'safety','offer') )
    rules_set.import_c45('data/car.c45', 'rs_car', 'offer')
    evaluations.evaluate('rs_car', 'ds_car')
    # this one isn't evaluated
    rules_set.import_apriori('data/rules.apriori','car.apriori')

  def tearDown(self):
    del self.cur
    conn.rollback()
    import os
    os.unlink('test.db')

  def test_01__evaluate(self):
    "[quality_measures] _evaluate"
    # TBD real testing here!
    quality_measures._evaluate("SUP", "nab/1728", 1, 1, 1, self.cur)
    
  def test_02_evaluate(self):
    "[quality_measures] evaluate & is_evaluated"
    quality_measures.evaluate("SUP", "rs_car", "ds_car")

    self.failIf(evaluations.is_evaluated("car.apriori", "ds_car"))
    quality_measures.evaluate("SUP", "car.apriori", "ds_car")
    self.failUnless(evaluations.is_evaluated("car.apriori", "ds_car"))

    # Those two are not compatible
    dataset.import_csv('data/abalone_with_headers.csv','abalone',',')
    self.assertRaises(ValueError,
                      quality_measures.evaluate, "SUP", "rs_car", "abalone")
    
  def test_03_is_computed(self):
    "[quality_measures] is_computed"
    self.assertRaises(ValueError,quality_measures.is_computed,
                      'quality', 'rs','ds')
    quality_measures.evaluate("SUP", "rs_car", "ds_car")
    self.failUnless(quality_measures.is_computed('SUP', 'rs_car','ds_car'))
    # should be case-insensitive
    self.failUnless(quality_measures.is_computed('sup', 'rs_car','ds_car'),
                    "is_computed() is not case-insensitive wrt measure's name")
    
  def test_04_computed_measures(self):
    "[quality_measures] computed_measures"
    self.failIf(quality_measures.computed_measures())
    quality_measures.evaluate("SUP", "rs_car", "ds_car")
    quality_measures.evaluate("SUP", "car.apriori", "ds_car")

    computed_measures=quality_measures.computed_measures()
    self.assertEqual(2, len(computed_measures))
    self.failUnless(('SUP', 'rs_car', 'ds_car') in computed_measures)
    self.failUnless(('SUP', 'car.apriori', 'ds_car') in computed_measures)


    self.assertRaises(ValueError,
                      quality_measures.computed_measures, 'hop', None)

    quality_measures.evaluate("CONF", "rs_car", "ds_car")
    computed_measures=quality_measures.computed_measures("rs_car", "ds_car")
    self.assertEqual(2, len(computed_measures))
    self.failUnless('CONF' in computed_measures)
    self.failUnless('SUP' in computed_measures)
    
  def test_05_has_computed_measures(self):
    "[quality_measures] has_computed_measures"
    self.failIf(quality_measures.has_computed_measures("rs_car","ds_car"))
    self.failIf(quality_measures.has_computed_measures("car.apriori","ds_car"))

    quality_measures.evaluate("SUP", "rs_car", "ds_car")
    quality_measures.evaluate("SUP", "car.apriori", "ds_car")
    quality_measures.evaluate("CONF", "rs_car", "ds_car")

    self.failUnless(quality_measures.has_computed_measures("rs_car","ds_car"))
    self.failUnless(quality_measures.has_computed_measures("car.apriori",
                                                           "ds_car"))
    self.assertRaises(ValueError,
                      quality_measures.has_computed_measures,
                      'I DO NOT EXIST', 'NEITHER DO I')

  def test_06_get_measures(self):
    "[quality_measures] get_measures"
    quality_measures.evaluate("PS", "rs_car", "ds_car")

    self.assertRaises(ValueError,
                      quality_measures.get_measures, 'CONF', 'BAD', 'ds_car')
    self.assertRaises(ValueError,
                      quality_measures.get_measures, 'CONF','ds_car','ds_car')

    ps_measures=quality_measures.get_measures('PS', 'rs_car', 'ds_car')
    self.assertEqual(rules_set.nb_rules('rs_car'), len(ps_measures))
    
    ps_measures=quality_measures.get_measures('ps', 'rs_car', 'ds_car')
    self.assertEqual(rules_set.nb_rules('rs_car'), len(ps_measures),
                     "get_measures is not case-insensitive wrt measure's name")

  def test_07_evaluate_IPD(self):
    "[quality_measures] measure: IPD"
    self.failIf(quality_measures.is_computed('INDIMP', 'rs_car','ds_car'))

    quality_measures.evaluate("IPD", "rs_car", "ds_car")
    self.failUnless(quality_measures.is_computed('INDIMP', 'rs_car','ds_car'))
    self.failUnless(quality_measures.is_computed('IPD', 'rs_car','ds_car'))

    ipd_measures=quality_measures.get_measures('IPD', 'rs_car', 'ds_car')
    self.assertEqual(rules_set.nb_rules('rs_car'), len(ipd_measures))
    
def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(Test_Quality_Measures, "test_"))
    return suite

if __name__ == "__main__":
    errs = utils.run_suite(test_suite())
    sys.exit(errs and 1 or 0)
