#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Tests for rules_set

"""
import unittest
if __name__ == "__main__":
  import utils, sys
  utils.fixpath()

from herbs.app import rules_set
from herbs.app import conn
from herbs.utils import log, FileFormatError

class Testrules_set(unittest.TestCase):
  """
  Tests for the rules_set
  """
  def setUp(self):
    conn.init('test.db', create=True)
    self.cur=conn.cursor()

  def tearDown(self):
    del self.cur
    import os
    os.unlink('test.db')
    
  def test_01_generic_import_apriori(self):
    "[Rulesmanager] import a priori"
    rules=[]
    fct=lambda rule: rule and rules.append(rule)
    rules_set.generic_import_apriori('data/rules.apriori', fct, discard_no_head=0)
    self.assertEqual(len(rules), 5)

    rules=[]
    rules_set.generic_import_apriori('data/rules.apriori', fct, discard_no_head=1)
    # check that the line with no head has been discarded
    self.assertEqual(len(rules), 4)
    self.assertEqual(rules[3]['head'],
                     "safety='high' and lug_boot='small' and offer='unacc'")
    self.assertEqual(rules[3]['tail'], "persons='2'")

  def _test_02_generic_import_apriori(self):
    "[Rulesmanager] import a priori"
    rules_set.import_apriori('data/rules.apriori', 'glop')

  def test_03_generic_import_c45_nosubtree(self):
    "[rules_set] generic import c4.5, no subtree"
    rules=[]
    fct=lambda rule: rule and rules.append(rule)
    rules_set.generic_import_c45('data/sample.c45', fct, 'class')
    self.assertEqual(len(rules), 8)
    self.assertEqual(rules[3]['head'],"safety='med' and persons='4' and buying='vhigh' and maint='high'" )
    self.assertEqual(rules[3]['tail'], "class='unacc'")

    self.assertRaises(FileFormatError, rules_set.generic_import_c45,
                      'data/sample_bad1.c45', fct, 'class')
    self.assertRaises(FileFormatError, rules_set.generic_import_c45,
                      'data/sample_bad2.c45', fct, 'class')
    self.assertRaises(FileFormatError, rules_set.generic_import_c45,
                      'data/sample_bad3.c45', fct, 'class')
    
  def test_04_generic_import_c45_with_subtrees(self):
    "[rules_set] generic import c4.5, with subtrees"
    rules=[]
    fct=lambda rule: rule and rules.append(rule)
    rules_set.generic_import_c45('data/abalone_little.c45', fct, 'class')
    self.assertEqual(len(rules), 12)

    # We here have no other choice but build some rules and check that they
    # were correctly parsed
    S3_root=" and ".join([
      "Shell_weight<='0.144'",
      "Diameter>'0.22'",
      "Sex='I'",
      "Shell_weight>'0.103'",
      "Height<='0.13'",
      "Shucked_weight>'0.1355'",
      "Length>'0.415'",
      "Length<='0.48'",
      "Shucked_weight<='0.223'",
      "Shucked_weight<='0.212'",
      "Shell_weight>'0.1145'",
      "Viscera_weight>'0.0735'",
      "Viscera_weight>'0.08'",
      "Height<='0.12'",
      "Height<='0.115'",
      "Diameter<='0.35'",
      ])
    
    S3={"Diameter>'0.345'" : "6",
        "Diameter<='0.345' and Diameter<='0.335'" : "7",
        "Diameter<='0.345' and Diameter>'0.335'" : "8",
        }
    #import pprint
    #pprint.pprint(rules)
    for expr in S3.keys():
      rule=rules_set.make_rule(S3_root+" and "+expr,"class='%s'"%S3[expr])
      #pprint.pprint(rule)
      self.failUnless(rule in rules)

  def test_05_create_rules_set(self):
    "[rules_set] create_rules_set() & rules_set()"
    self.assertEqual({}, rules_set.rules_sets())
    id=rules_set.create_rules_set("test_05")
    self.assertEqual(1, id)
    self.failUnless(rules_set.rules_sets().keys()==['test_05'])

  def test_06_delete_rules_set(self):
    "[rules_set] delete_rules_set"
    id=rules_set.create_rules_set('test_06')
    self.assertEqual(1, id)
    rules_set.delete_rules_set('test_06')
    self.failUnless(rules_set.rules_sets().keys()==[])

    self.assertRaises(ValueError,
                      rules_set.delete_rules_set, 'does_not_exists')

  def test_07__id(self):
    "[rules_set] _id"
    rules_set.import_apriori('data/rules.apriori', 'test_07')
    self.assertEqual(1, rules_set._id('test_07'))

  def test_08_rules_named(self):
    "[rules_set] rules_named"
    rules_set.import_apriori('data/rules.apriori', 'test_08')
    rules = rules_set.rules_named('test_08') # returns a generator
    # 4 rules out of 5 lines since parameter discard_no_head defaults to True
    self.assertEqual(4, len(list(rules)))
    #print [r for r in rules_set.rules_named('test_08')]
    ids=[r['id'] for r in rules]
    sorted_ids=list(ids) ; sorted_ids.sort()
    self.assertEqual(ids, sorted_ids)

  def test_09__rules_id_named(self):
    "[rules_set] _rules_id_named"
    rules_set.import_c45('data/car.c45', 'test_09', 'offer')
    # check that it returns a list of integers, as expected
    ids=rules_set._rules_id_named('test_09')
    self.failUnless(type(ids)==type([]))
    self.failUnless(type(ids[0])==type(1))

    self.failUnless( len(ids) > 10 )
    sorted_ids=list(ids) ; sorted_ids.sort()
    self.assertEqual(ids, sorted_ids)

    ids=rules_set._rules_id_named('test_09', limit=10)
    self.failUnless(len(ids)==10)
    sorted_ids=list(ids) ; sorted_ids.sort()
    self.assertEqual(ids, sorted_ids)

    self.assertRaises(ValueError,
                       rules_set._rules_id_named, 'it_does_not_exist')

  def test_10_attributes(self):
    "[rules_set] attributes"
    attrs=['offer','doors','buying','safety','maint','persons','lug_boot']
    attrs.sort()

    # c4.5
    rules_set.import_c45('data/car.c45', 'test_10a', 'offer')
    observed_attrs=rules_set.attributes('test_10a')
    observed_attrs.sort()
    self.assertEqual(attrs, observed_attrs)

    # apriori
    attrs.remove('doors')
    rules_set.import_apriori('data/rules.apriori', 'test_10b')
    observed_attrs=rules_set.attributes('test_10b')
    observed_attrs.sort()
    self.assertEqual(attrs, observed_attrs)

    # Error handling
    self.assertRaises(ValueError, rules_set.attributes, 'does_not_exist')

  def test_13__rules_set_for_rule_id(self):
    "[rules_set] _rules_set_for_rule_id"
    rules_set.import_apriori('data/rules.apriori','test_13')
    # 4 rules out of 5 lines since parameter discard_no_head defaults to True
    rules_id=rules_set._rules_id_named('test_13')
    self.assertEqual(4, len(rules_id))
    for rule_id in rules_id:
      self.assertEqual('test_13', rules_set._rules_set_for_rule_id(rule_id))

    self.assertRaises(RuntimeError, rules_set._rules_set_for_rule_id, 999)

  def test_14_nb_rules(self):
    "[rules_set] nb_instances"
    self.assertRaises(ValueError, rules_set.nb_rules, 'foobar')

    rules_set.import_apriori('data/rules.apriori', 'car_apriori')
    self.assertEqual(4, rules_set.nb_rules('car_apriori'))
    self.assertEqual(type(1), type(rules_set.nb_rules('car_apriori')))

  def test_15_import_does_not_delete_existing_rs(self):
    "[rules_set] import does not delete  an existing rules set"
    rules_set.import_apriori('data/rules.apriori','test_15')
    self.failUnless(rules_set.exists('test_15'))
    self.assertRaises(ValueError, rules_set.create_rules_set, 'test_15')
    self.assertRaises(ValueError, rules_set.create_rules_set, 'test_15')
    self.assertRaises(ValueError, rules_set.import_apriori,
                      'data/rules.apriori','test_15')
    self.failUnless(rules_set.exists('test_15'))
    self.assertRaises(ValueError, rules_set.import_apriori,
                      'data/rules.apriori','test_15')

def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(Testrules_set, "test_"))
    return suite


if __name__ == "__main__":
  if '-v'  in sys.argv:
    import logging
    log.setLevel(logging.NOTSET)
  errs = utils.run_suite(test_suite())
  sys.exit(errs and 1 or 0)

"""
 and  and  and  and  and  and  and  and  and  and  and  and Diameter <= 0.315 : 6 (5.0/3.0)
 and  and  and  and  and  and  and  and  and  and  and  and Diameter > 0.315 :[S2]
"""
