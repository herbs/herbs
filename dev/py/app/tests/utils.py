# -*- coding: iso-8859-1 -*-
"""
Helper functions for the test suite.

Extracted from the Modeling Framework http://modeling.sourceforge.net
by Sebastien Bigaret.

This file was inspired by TAL tests.utils
"""

__version__='$Revision: $'[11:-2]

import os, sys

# 
# mydir is where this file is, codedir is 2 level up
def fixpath():
  mydir = os.path.abspath(os.path.dirname(__file__))
  codedir = os.path.dirname(os.path.dirname(mydir))
  codedir = os.path.dirname(codedir)
  if codedir not in sys.path:
    sys.path=[codedir]+sys.path

import unittest

def run_suite(suite, outf=sys.stdout, errf=None, verbosity=0):
    """
    Runs the suite.
    Verbosity: 0: not verbose, 1: just print points, >1: verbose
    """
    runner = unittest.TextTestRunner(outf, verbosity=verbosity)
    result = runner.run(suite)

    newerrs = len(result.errors) + len(result.failures)
    if newerrs:
        print "'Errors' indicate exceptions other than AssertionError."
        print "'Failures' indicate AssertionError"
        if errf is None:
            errf = sys.stderr
        errf.write("%d errors, %d failures\n"
                   % (len(result.errors), len(result.failures)))
    return newerrs
