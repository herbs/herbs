#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Dev. utilities for herbs
"""
import os, sys

def fixpath():
  """
  Prepends directory ../.. to sys.path, so that any import from herbs
  imports the modules in this directory, rather then the possibly installed
  one: we want to test the framework in this directory.

  """
  # mydir is where this file is, codedir is 1 level up
  mydir = os.path.abspath(os.path.dirname(__file__))
  codedir = os.path.dirname(mydir)
  if codedir not in sys.path:
    sys.path=[codedir]+sys.path
