#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""HERBS

"""
from distutils.core import setup
import glob, os, sys

# Instruction for PyPi found at:
# http://www.python.org/~jeremy/weblog/030924.html
# TBD classifiers
classifiers = """\
Development Status :: 4 - Beta
Intended Audience :: Developers
Operating System :: OS Independent
Programming Language :: Python
"""
if sys.version_info < (2, 3):
    _setup = setup
    def setup(**kwargs):
        if kwargs.has_key("classifiers"):
            del kwargs["classifiers"]
        _setup(**kwargs)

doclines = __doc__.split("\n")
short_description = doclines[0]
long_description = "\n".join(doclines[2:])

setup(name="HERBS",
      version="0.1",
      license ="",
      description=short_description,
      author="Sébastien Bigaret",
      author_email="sbigaret@users.sourceforge.net",
      maintainer="Sebastien Bigaret",
      maintainer_email="sbigaret@users.sourceforge.net",
      url="http://modeling.sourceforge.net/",
      classifiers = filter(None, classifiers.split("\n")),
      package_dir={'herbs':'.'},
      packages=[ 'herbs', 'herbs.app', 'herbs.GUI' ],
      long_description = long_description,
      scripts = ['herbscmd.py', 'GUI/wx_herbs.py' ]
     )

