#! /usr/bin/env python
# -*- coding: iso-8859-1 -*-
## LICENSE
"""
Utilities for herbs
"""
import logging, new, os
from logging import config

TRACE=logging.TRACE=5
logging.addLevelName(TRACE, 'TRACE')
#config.fileConfig(os.path.join( os.path.abspath(os.path.dirname(__file__)),
#                                os.path.expanduser('~/log.ini')) )

class HerbsLogger(logging.Logger):
  def trace(self, msg, *args, **kw):
    self.log(TRACE, msg, *args, **kw)

# cf. findCaller
l=HerbsLogger.log.im_func.func_code
f=HerbsLogger.trace.im_func.func_code
c=new.code(f.co_argcount, f.co_nlocals, f.co_stacksize, f.co_flags,
           f.co_code, f.co_consts, f.co_names, f.co_varnames,
           l.co_filename, f.co_name, f.co_firstlineno, f.co_lnotab)
del f, l
trace=new.function(c, HerbsLogger.trace.im_func.func_globals)
HerbsLogger.trace=new.instancemethod(trace,None, HerbsLogger)

logging.setLoggerClass(HerbsLogger)
log = logging.getLogger('herbs')
log.addHandler(logging.StreamHandler())
log.setLevel(logging.INFO)
del config, logging, new, trace

class FileFormatError(Exception):
  pass

def sql_normalize(name):
  """

  For now, simply replace spaces with underscores. This definitely needs to be
  changed. For example, conversion from latin1 to plain ascii is a topic
  addressed in the python cookbook at:
  http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/251871

  """
  return name.replace(' ', '_')

def openext(path, mode='r', buffering=0):
  """
  ''Smartly'' opens the file
  """
  if os.path.splitext(path)[1]=='.bz2':
    log.debug('Opening bz2 file %s', path)
    import bz2
    return bz2.BZ2File(path, mode, buffering)
  
  if os.path.splitext(path)[1]=='.gz':
    log.debug('Opening gzip file %s', path)
    import gzip
    return gzip.GzipFile(path, mode)
  
  log.debug('Opening regular file %s', path)
  return open(path, mode, buffering)
