.. -*- mode: rst -*-
.. compile into html with:
.. python docutils/tools/rst2html.py --output-encoding=iso-8859-1 traitements.rst traitements.html
.. Use rst2latex.py for tex, ps & pdf

==================================
Cha�nage de traitements dans HERBS
==================================

:Author: Beno�t Vaillant
:Date: 2004/08/05

.. :Created: 2004/08/05

Ce document a pour but de d�crire les diff�rents traitements �
impl�menter dans HERBS, en termes d'entr�es/sorties, afin de pouvoir
effectuer des cha�nes de traitements complexes � partir de traitements
simples.

Sans indices
------------

Filtrage d'une base de cas
^^^^^^^^^^^^^^^^^^^^^^^^^^

Entr�es
  un base de cas bdc.

Sorties
  une base de cas, incluse dans bdc.

Filtrage d'une base de r�gles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Entr�es
  un couple (bdc,bdr) compatibles.

Sorties
  une base de r�gles, incluse dans bdr.


Mono-indices
------------

Analyse
^^^^^^^

Entr�es
  un couple (bdc,bdr) compatibles

Sorties
  deux sorties sont � consid�rer :

  - une cha�ne de caract�re donnant des informations sommaires sur
    l'entr�e (se r�f�rer au fichier sur les traitements_ pour plus de
    renseignements quand aux informations � retourner),
  - le couple (bdc,bdr) � l'identique de celui d'entr�e, en vue de
    traitements ult�rieurs.

.. _traitements : ./traitements.rst

N meilleures solutions
^^^^^^^^^^^^^^^^^^^^^^

Param�tres
  N (entier)

Entr�es
  un couple (bdc,bdr) compatibles
  une mesure de qualit�

Sorties
  une base de r�gles

Taux d'erreur
^^^^^^^^^^^^^

Param�tres

Entr�es
  un couple (bdc,bdr) compatibles

Sorties
  un tableau


Multi-indices
-------------

Ensemble de r�gles class�es k fois parmi les N meilleures par plusieurs indices
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Param�tres
  N,k (entiers)

Entr�es
  un couple (bdc,bdr) compatibles
  un ensemble de mesures de qualit�

Sorties
  une base de r�gles

Comparaison de pr�ordres induits par deux indices
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Param�tres

Entr�es
  un couple (bdc,bdr) compatibles
  deux mesures de qualit�

Sorties
  tau_1 (r�el entre 0 et 1)
