.. -*- mode: rst -*-
.. compile into html with:
.. python docutils/tools/rst2html.py --output-encoding=iso-8859-1 traitements.rst traitements.html
.. Use rst2latex.py for tex, ps & pdf

=================
Traitements HERBS
=================

:Author: S�bastien Bigaret
:Date: 2004/08/03

.. :Created: 2004/07/29 

_________________________

Sommaire
--------
1. Notations_
2. Mesures_
3. `Sans mesures`_
4. `Mono-mesures`_
5. `Multi-mesures`_


Notations
---------

Dans la suite du document, on utilisera les notations suivantes :
  - ``bdc`` correspond � une base de cas donn�e,
  - ``bdr`` correspond � une base de r�gle donn�e,
  - ``n`` est le nombre de cas (nombre d'enregistrements de bdc)
  - ``na`` correspond au nombre de cas v�rifiant la pr�misse d'une r�gle
  - ``nb`` correspond au nombre de cas v�rifiant la conclusion d'une
    r�gle
  - ``nab`` correspond au nombre d'exemples d'une r�gle
  - ``nab_`` correspond au nombre de contre-exemples d'une r�gle

Mesures
-------

coefficient de corr�lation lin�aire (R)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

 (n*nab-na*nb)/sqrt(n*na*nb*(n-na)*(n-nb))

Cas particuliers::

  na=0 ou n, nb=0 ou n

+--------------+--------------+--------------+--------------+
|              |  ``na = 0``  |  ``na = n``  |``na <> 0,n`` |
+--------------+--------------+--------------+--------------+
|  ``nb = 0``  |      ?       |       ?      |      ?       |
+--------------+--------------+--------------+--------------+
|  ``nb = n``  |      ?       |       ?      |      ?       |
+--------------+--------------+--------------+--------------+
| ``nb <> 0,n``|      ?       |       ?      |      R       |
+--------------+--------------+--------------+--------------+



confiance (CONF)
^^^^^^^^^^^^^^^^

D�finition::

  nab/na

Cas particuliers::

  na=0

confiance centr�e (CONFCEN)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  (n*nab-na*nb)/(n*na)


conviction (CONV)
^^^^^^^^^^^^^^^^^

D�finition::

  (na*(n-nb))/(n*nab_)

indice d'implication (INDIMP)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  (na*nb-n*nab)/sqrt(n*na*(n-nb))

**Attention !**
  Pour mesurer la qualit� d'une r�gle, c'est -INDIMP qu'il faut
  consid�rer !

indice probabiliste discriminent (IPD) [#]_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

   P[N(0,1) > INDIMP^{CR/bdr}]

Dans ``herbs.pl``, le calcul est simplifi� et correspond uniquement �
INDIMP car les pr�ordres ne sont pas affect�s par les autres
transformations.

Afin de calculer IPD il faut proc�der en trois �tapes :
  - Calculer la valeur de INDIMP pour toutes les r�gles (on note ici N
    le nombre de r�gles dans bdr)

  - Centrer/R�duire cet ensemble de valeurs :
      Centrer/R�duire revient � faire la transformation lin�aire::

         INDIMP^{CR/bdr}=(INDIMP-moy)/sigma

      o� ``moy`` est la moyenne de INDIMP pour le couple (bdc, bdr)
      �tudi�, et ``sigma`` l'�cart-type::

        moy=sum(INDIMP)/N
        
        sigma=sqrt(sum((INDIMP-moy)^2)/N)

  - Comparer cette valeur � la loi normale centr�e-r�duite::

      IPD=1-Phi(INDIMP^{CR/bdr})
      
      o� ``Phi`` est la fonction de r�partition de la loi normale
      centr�e r�duite.

indice de qualit� de Cohen (IQC) [#]_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  2*(n*nab-na*nb)/((n-na)*nb+na*(n-nb))

intensit� d'implication (INTIMP)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  P[ Poisson(na*(n-nb)/n) >= nab_ ]

Dans ``herbs.pl`` le calcul est fait en fonction de:

  - comparaison avec la loi normale (valeurs cach�es dans un tableau constant)
  - IndImp

� mettre en rapport avec : IIC dans ``externals/progs/intImp/calculIntensite.c``
(IIC = Intensite d'Implication Classique)

intensite d'implication entropique (IIE)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  IIE = ... 

voir papier de r�f�rence (Lenca, Meyer, Vaillant, Picouet, Lallich)

Dans ``herbs.pl`` le calcul est fait en fonction de:

  - INTIMP
  - coeff. entropique (tests, log2, puissance)

� mettre en rapport avec: IIE dans ``externals/progs/intImp/calculIntensite.c``

gain informationnel (GI)
^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

   log((n*nab)/(na*nb))

lift (LIFT)
^^^^^^^^^^^

D�finition::

  (n*nab)/(na*nb)

mesure de Laplace (LAP)
^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

 (nab+1)/(na+2)


mesure de Loevinger (LOEV)
^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  (n*nab-na*nb)/[na*(n-nb)]

mesure de Piatetsky-Shapiro (PS)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  (n*nab-na*nb)/n�


mesure de Sebag et Schoenauer (SEBAG)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

 nab/nab_

mesure de Zhang (ZHANG)
^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  (n*nab-na*nb)/max{nab*(n-nb);nb*nab_}

moindre contradiction (MOCO) [#]_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  (nab-nab_)/nb

multiplicateur de cotes (MC)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

 (nab*(n-nb))/({nb*nab_)

support (SUP)
^^^^^^^^^^^^^

D�finition::

  nab/n

taux d'exemples et de contre-exemples (TEC)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

D�finition::

  (nab-nab_)/nab


----------

.. [#] INDIMP^{CR/bdr} correspond � INDIMP centr�
       r�duit (CR) pour une base de r�gles admissible bdr.
.. [#] sa d�nomination dans la litt�rature anglaise est Kappa.
.. [#] pr�c�demment nomm�e surprise (SURP)


Sans mesures
------------

filtrage d'une base de cas
^^^^^^^^^^^^^^^^^^^^^^^^^^

Filtrer une base de cas permet de s�lectionner une sous-partie de
cette base de cas. Le but peut �tre d'�liminer des cas probl�matiques
(valeurs non significatives, sur-repr�sentativit� de certaines
valeurs...)

**TODO :**
  � pr�ciser

filtrage d'une base de r�gles
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le filtrage d'une base de r�gles (bdr) permet de s�lectionner en
fonction de divers crit�res une partie des r�gles qu'elle contient,
pour une base de cas (bdc) donn�e.

Nous retenons deux crit�res principaux :
  - le _`filtrage logique`.
       dans ce cas, filtrer bdr revient � ne conserver que les r�gles
       qui font sens, *i.e.* celles dont la d�finition en fonction des
       donn�es est admissible. on �liminera donc de bdr toutes les
       r�gles n'ayant pas d'exemple sur bdc (``nab=0``).
  - le filtrage statistique.
       dans ce cas, le filtrage de bdr se fait selon un crit�re pus
       fort que dans le cas du `filtrage logique`_. **Dans le cadre
       des r�gles d'associations**, il est couremment admis qu'une
       r�gle est potentiellement int�ressante � partir du moment o� ::

			   na*nb < n*nab

       on peut vouloir chercher � s'�loigner suffisemment du cas
       limite d'�galit�. Une m�thode classique est de filtrer la base
       de r�gle en imposant un seuil mimnimal pour le lift [#]_. Pour
       une valeur de seuil de 1, on se trouve dans le cas ``na*nb <
       n*nab``.

.. [#] le lift est ici � consid�rer comme moyen de filtrage et non comme
       mesure de qualit� !

Mono-mesures
------------

analyse simple
^^^^^^^^^^^^^^

Le but de l'`analyse simple`_ est de retourner quelques grandeurs
d�crivant l'ad�quation d'une base de cas et d'une base de r�gles
compatibles.  Les grandeurs cl�s sont :

  - le nombre de cas de la base de cas (``n``).
  - le nombre de r�gles de la base de r�gles.
  - le taux de couverture (il est d�fini comme la proportion de cas de
    la base de cas ayant au moins une r�gle pr�disant l'un de leurs
    attributs).
  - le taux de recouvrement (il est d�fini comme le nombre moyen de
    r�gles pr�disant l'un de attributs de chaque cas, moins 1, pour
    tous les cas couverts, *i.e.* pour lesquels il existe au moins une
    r�gle pr�disant l'un de leurs attributs)
  - le nombre de r�gles ''particuli�res'' :
      - le nombre de r�gles logiques (``nab_=0``).
      - le nombre de r�gles sans exemples (``nab=0``).
      - le nombre de r�gles ne passant pas l'hypoth�se d'ind�pendance
        (*i.e.* telles que ``na*nb >= n*nab``).

Exemple de r�sultats::

    Analyse de la base de r�gles autompg_apriori pour la base de cas autompg :
    
    Nombre de cas : 392
    Nombre de r�gles : 666
    Taux de couverture : 97.19 %
    Taux de recouvrement : 11.1574803149606
    R�gles particuli�res :
     * 565 r�gles logiques
     * 18 r�gles sans exemples
     * 18 r�gles ne passant pas l'hypoth�se d'ind�pendance (dont 18 pour lesquelles na*nb=nab*n)

N meilleures r�gles
^^^^^^^^^^^^^^^^^^^

Le but des `N meilleures r�gles`_ est de retourner, �tant donn� une
mesure de qualit� donn�e et un couple (bdc,bdr), un ensemble de r�gles
de bdr les mieux class�es par la mesure, la taille minimale de cet
ensemble �tant param�tr�e par l'entier ``N``.

Pour N fix�, on range les r�gles en fonction de la valeur de la mesure
de qualit�, et on retourne un sous-ensemble de r�gles de bdr tel que
le rang de chacune des r�gles du sous-ensemble soit inf�rieur ou �gal
� N (par rang on entend le num�ro de rangement de la r�gle : la mieux
class�e est de rang 1, la seconde de rang 2, etc [#]_).

Exemple de r�sultats::

  Base de cas : autompg
  Base de r�gles : autompg_apriori
  Indice : support
  Valeur de N : 11 (valeur initialement demand�e : 8)
  
  +------+-----------------+--------------------+-----------------+
  | rang | Num�ro de r�gle | Valeur de l'indice | H.I. v�rifi�e ? |
  +------+-----------------+--------------------+-----------------+
  | 1    | r�gle 189       | 0.262755           | oui             | na=103, nb=245, nab=103, nab_=0
  | 2    | r�gle 188       | 0.186224           | oui             | na=83, nb=245, nab=73, nab_=10
  | 3    | r�gle 187       | 0.176020           | oui             | na=79, nb=199, nab=69, nab_=10
  | 4    | r�gle 186       | 0.155612           | oui             | na=68, nb=199, nab=61, nab_=7
  | 5    | r�gle 171       | 0.073980           | oui             | na=31, nb=245, nab=29, nab_=2
  | 6    | r�gle 185       | 0.068878           | oui             | na=30, nb=199, nab=27, nab_=3
  | 7    | r�gle 184       | 0.058673           | oui             | na=27, nb=199, nab=23, nab_=4
  | 8    | r�gle 182       | 0.056122           | oui             | na=22, nb=103, nab=22, nab_=0
  | 8    | r�gle 183       | 0.056122           | oui             | na=22, nb=245, nab=22, nab_=0
  | 8    | r�gle 502       | 0.056122           | oui             | na=22, nb=245, nab=22, nab_=0
  | 8    | r�gle 503       | 0.056122           | oui             | na=22, nb=103, nab=22, nab_=0
  +------+-----------------+--------------------+-----------------+

.. [#] la gestion d'ex-aequos se fait de mani�re naturelle : si il y a
       ``p`` r�gles de rang ``m``, le rang assign�e � la (ou les)
       r�gle suivante est ``m+p``.

taux d'erreur
^^^^^^^^^^^^^

Le taux d'erreur (en %) est d�fini comme suit::

  nab_/na*100

**TODO :**
  Lorsque ``na=0``, quelle valeur ? ``0`` ou ``+inf`` ?

R�sultats::

  Base de cas : autompg
  Base de r�gles : autompg_apriori
  
  +-----------------+-------------------+
  | Num�ro de r�gle | Taux d'erreur (%) |
  +-----------------+-------------------+
  | 1               | 0.00              |
  | 2               | 0.00              |
  | 3               | 0.00              |
  | 4               | +inf              |
  | 5               | +inf              |
  | 6               | 0.00              |
  | 7               | 0.00              |
  |        [...]    |          [...]    |
  | 662             | 0.00              |
  | 663             | 20.00             |
  | 664             | 0.00              |
  | 665             | 0.00              |
  | 666             | 0.00              |
  +-----------------+-------------------+
  | Moyenne         | +inf              |
  +-----------------+-------------------+
  | Moy. sans +inf  | 2.82418209876543  |
  +-----------------+-------------------+


distribution des valeurs d'une mesure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'analyse d'une mesure de qualit� peut se faire en �tudiant les
valeurs prises par cette mesure sur un couple (bdc,bdr) donn�.

On souhaite dans ce cas tracer les valeurs prises par la mesure pour
chacune des r�gles de bdr, en ordonn�e. Afin d'obtenir une
repr�sentation en deux dimension, on peut utiliser diverses grandeurs
en abscise.

pour chaque r�gle r de bdr, m �tant la mesure que l'on souhaite
�tudier, on placera un point selon :

  - la taille de la pr�misse (``na``) ::

     m ^
       |
       |    + (na, m(r))
       |
       |
       |
       |
       +------------> na

  - la taille de la conclusion (``nb``) ::

     m ^
       |
       |    + (nb, m(r))
       |
       |
       |
       |
       +------------> nb

  - le nombre d'exemples (``nab``) ::

     m ^
       |
       |    + (nab, m(r))
       |
       |
       |
       |
       +------------> nab

  - le nombre de contre-exemples (``nab_``) ::

     m ^
       |
       |    + (nab_, m(r))
       |
       |
       |
       |
       +------------> nab_


Certaines mesures ayant des valeurs extr�mes infinies, on doit avoir
le choix de transformer l'axe des ordonn�es de mani�re
non-lin�aire. Voir pour cel� la `note sur le changement d'�chelle`_ en
fin de document.

.. Note:: Il est bien �videmment possible que deux r�gles aient les
     m�mes coordonn�es. Il peut �tre souhaitable de repr�senter la
     "densit� ponctuelle" sur le graphique au moyen d'un syst�me de
     couleurs.

**TODO :**
  � pr�ciser

Multi-mesures
-------------

ensemble de r�gles class�es k fois parmi les N meilleures par plusieurs mesures
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Le but est de retourner, �tant donn� ``p`` mesures de qualit� et un
couple (bdc,bdr), un ensemble de r�gles de bdr class�es au moins ``k``
fois par les ``p`` mesures dans l'ensemble des `N meilleures r�gles`_.

Pour ``k=1`` par exemple, on obtient l'union des ``p`` ensembles des
``N`` meilleures r�gles, et pour ``k=p``, leur intersection.

.. Note:: Il peut �tre int�ressant de pouvoir trier les r�sultats soit
     sur l'identifiant de la r�gle, soit sur le nombre de fois ou elle
     est s�lectionn�e.

Exemple de r�sultats (base de cas: ``car``, bases de r�gles: ``car_c4_5``)::

  Indices : support, confiance, intensiteImplication
  N=10, k=2
  
  r�gle 1 class�e 3 fois
  r�gle 2 class�e 3 fois
  r�gle 3 class�e 2 fois
  r�gle 4 class�e 2 fois
  r�gle 11 class�e 2 fois
  r�gle 20 class�e 3 fois
  r�gle 30 class�e 3 fois
  r�gle 40 class�e 2 fois
  r�gle 41 class�e 2 fois
  r�gle 82 class�e 3 fois
  r�gle 83 class�e 2 fois
  r�gle 84 class�e 2 fois
  r�gle 85 class�e 3 fois
  r�gle 86 class�e 3 fois
  r�gle 87 class�e 2 fois
  r�gle 88 class�e 3 fois
  r�gle 89 class�e 3 fois
  r�gle 90 class�e 3 fois
  r�gle 91 class�e 3 fois
  r�gle 92 class�e 3 fois
  r�gle 99 class�e 3 fois
  r�gle 109 class�e 2 fois
  r�gle 110 class�e 2 fois
  r�gle 111 class�e 3 fois
  r�gle 113 class�e 2 fois
  
  -------------------------
  Total : 25 r�gles sur 134

qB: ``k`` entre 1 et nb. d'indices

comparaison de pr�ordres induits par deux mesures
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

(extrait d'un certain chapitre III. �l�ments de r�ponses pratiques,
�1.2. Comparaison de pr�ordres totaux)

Consid�rons deux r�gles ``r1`` et  ``r2`` et deux mesures/indices ``m1`` et
``m2``.

selon les valeurs ``m1(r1)``, ``m1(r2)``, ``m2(r1)`` et ``m2(r2)``, et ``m2``
sur ``r1`` et ``r2``, une de ces 9 cases est valide:

+-------------+--------+------------------------------+
|             | ``m1`` | ``m1(r1)`` ? ? ? ``m1(r2)``  |
+-------------+--------+---------+---------+----------+
|   ``m2``    |        |  ``<``  |  ``=``  | ``>``    |
+-------------+--------+---------+---------+----------+
|``m2(r1)``   | ``<``  |    a    |    b2   |    d     |
|?            +--------+---------+---------+----------+
|?            | ``=``  |    b1   |    c    |    b1    |
|?            +--------+---------+---------+----------+
|``m2(r2)``   | ``>``  |    d    |    b2   |    a     |
+-------------+--------+---------+---------+----------+

On obtient les valeurs ``a``, ``b1``, ``b2``, ``c`` et ``d`` pour les mesures
``m1`` et ``m2`` en faisant varier ``r1`` et ``r2`` sur toute une base de
r�gles.

� partir de ces grandeurs, on peut d�finir des coefficients d'accord entre
pr�ordres totaux (Giakoumakis et Monjardet). Exemple::

  tau_1 = [ 2(a-d) + 2(c*-b) ] / [ n(n-1) ]

avec:

- ``a`` nombre d'accords stricts
- ``c`` nombre d'accords larges,
- ``2d`` nombre de d�saccords stricts
- ``b=b1+b2`` nombre de semi-accords (ou nombre de semi-d�saccords)
- ``c*=(c-n)/2`` avec ``n`` le nombre de r�gles. 

.. Note:: si les coefficients d'accord calcul�s ne sont pas fonctions de
     ``b1`` et ``b2`` mais de ``b``, on peut acc�l�rer le calcul d'un facteur
     2 en faisant varier les r�gles ``ri, rj`` avec ``i<=j``. De plus, pour
     ``i=j`` on est forc�ment dans la case correspondant au coefficient ``c``. 

Exemple de r�sultats::

  Indices : confiance, IQC
  Base de cas : car
  Base de r�gles : car_c4_5
  Tau : 0.044720008977668
       a
  Tau : -0.184603299293009
       1
  Debug : a=2827, b=3248, c=1746, c*=806, d=2030, n=134



distribution des valeurs de deux mesure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

L'analyse de deux mesures peut se faire en croisant les valeurs prises
par ces mesure pour un couple (bdc,bdr) donn�.

On souhaite dans ce cas tracer les valeurs prises par la mesure m1
pour chacune des r�gles de bdr en abscisse, et celles prises par la
mesure m2 en ordonn�es.

pour chaque r�gle r de bdr, on placera un point dans le plan comme
suit ::

    m2 ^
       |
       |    + (m1(r),m2(r))
       |
       |
       |
       |
       +------------> m1

Certaines mesures ayant des valeurs extr�mes infinies, on doit avoir
le choix de transformer les �chelles de mani�re non-lin�aire. Voir
pour cel� la `note sur le changement d'�chelle`_ en fin de document.

.. Note:: Il est bien �videmment possible que deux r�gles aient les
     m�mes coordonn�es. Il peut �tre souhaitable de repr�senter la
     "densit� ponctuelle" sur le graphique au moyen d'un syst�me de
     couleurs.

**TODO :**
  � pr�ciser

note sur le changement d'�chelle
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Afin de repr�senter graphiquement les valeurs prises par une mesure,
on peut souhaiter effectuer des changements d'�chelle non-lin�aires
sur les axes.

Parmis les diverses transformations, on retiendra :

 - l'�chelle logarithmique :
      dans ce cas, ce n'est plus la valeur de la mesure, mais le
      logarithme de cette valeur qui sera tra�� **Attention aux
      mesures prenant des valeurs n�gatives !**

 - l'�chelle ?? *(je ne sais pas si �a a un nom, je vais chercher...)* :
      dans ce cas, on ne trace pas la valeur de la mesure m, mais la
      grandeur ``m/(1+m^2)``.

.. Note:: lorsqu'un seul des axes d'un graphique a une �chelle
     logarithmique on parle de repr�sentation logarithmique, et
     lorsque les deux axes sont en �chelle logarithmique, on parle de
     repr�sentation logarithmique sans pr�cision aucune. Il peut �tre
     utile de pr�ciser l'axe logarithmique dans les repr�sentations
     semi-logarithmiques, ceci semble se faire en parlant de
     repr�sentation semi-logarithmique x lorsque l'axe logarithmique
     est l'axe des abscisses, et semi-logarithmique y lorsque l'axe
     logarithmique est l'axe des ordonn�es.
