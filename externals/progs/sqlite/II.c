#include <stdio.h>
#include <math.h>
#include "Phi_table.h"
#include "II.h"

// log2 now in glibc TODO configure

double
implication_index(long int n, long int na, long int nb, long int nab)
{
  double pab = (double) nab / (double) n;
  double pa = (double) na / (double) n;
  double pb = (double) nb / (double) n;

  double p_b = 1 - pb;
  double pa_b = pa - pab;

  if ((pa * p_b) == 0.0) { return 100.0; }
  else { return (sqrt ((double) n) * ( (pa_b - pa*p_b) / sqrt(pa * p_b) ));}
  
}


double
phi(double x)
{
  int i;
  double phi_x;
  
  //i = (int) ( (x + 1.0e-3) * 1.0e2 );
  i = (int) ( fabs(x) * CDF_X_TO_INDEX_COEFF  );

  /* fabs(x) >= 6.0 -> return 1 */

  if (i > CDF_MAX_INDEX)
	phi_x = 1.0;
  else
	phi_x = CUMULATIVE_DISTRIBUTION_FUNCTION[i];
  
  /* Phi(-x) + Phi(x) = 1 */
  if (x < 0.0)
	phi_x = 1.0 - phi_x;
  return phi_x;
}


double 
entropic_coeff(long int n, long int na, long int nb, long int nab)
{
  double h1, h2, iab, produit_h1_h2;
  double pa, pb_, pab_;
  double N = (double) n;
  double Na = (double) na;
  double Nab = (double) nab;
  double Nb = (double) nb;
  
  pa = Na /  N;
  pb_ = 1.0 - (Nb /  N);
  pab_ = pa - (Nab /  N);

  if ((0 <= pab_) && (pab_ < pa/2.0))
    { h1 = - (1- pab_ / pa) * log2 (1- pab_ / pa) - (pab_ / pa) * log2 (pab_ / pa); }
  else { h1 = 1.0; }
  
  if ((0 <= pab_) && (pab_ < pb_/2.0))
    { h2 = - (1- pab_ / pb_) * log2 (1- pab_ / pb_) - (pab_ / pb_) * log2 (pab_ / pb_); }
  else { h2 = 1.0; }
  
  produit_h1_h2 = (1.0 - pow(h1, 2)) * (1.0 - pow(h2, 2));
  iab = pow (produit_h1_h2, 0.25);

  return (iab);
} /* End of coeff_entropique */


double
II(long int n, long int na, long int nb, long int nab)
{
  return phi(implication_index( n, na, nb, nab ));
}


double
EII(long int n, long int na, long int nb, long int nab)
{
  return sqrt( II(n, na, nb, nab) * entropic_coeff(n, na, nb, nab) );
}

double
IPD(double x, double mean, double std_dev)
{
  return 1-phi( (x-mean)/std_dev );
}

/* static functions */
/* see comment at top */
/*
static double
log2(double x) { return (log(x) / log(2.0)); }
*/
