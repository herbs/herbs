Building sqlite
---------------

- get the sources, C file::

    wget http://sqlite.org/2015/sqlite-src-3081002.zip

Puis::

  unzip sqlite-src-3081002.zip && cd sqlite-src-3081002
  cp ../II.h ../II.c ../Phi_table.h ./src/
  patch -p1 < ../sqlite-src-3081002.patch
  ./configure --prefix=/usr/local  # adapt to your needs
  make
  make install # optional, or make LD_LIBRARY_PATH point to the libs so that..

... python sqlite3 uses the installed sqlite3, cf for example::

  ldd /usr/lib/python2.7/lib-dynload/_sqlite3.so
        linux-vdso.so.1 =>  (0x00007fffb8ffc000)
        libsqlite3.so.0 => /usr/local/lib/libsqlite3.so.0 (0x00007f90752ac000)
        libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f9075090000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f9074d04000)
        libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f9074aee000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f907486c000)
        libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f9074667000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f90757a3000)
