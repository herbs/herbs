#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

/**
 *
 */
double
phi_std(double x)
{
  return (1+erf(x*0.70710678118654746)) / 2;
}

/**
 */
void
gen_Phi_table(int max, int nb_steps)
{
  double x=0; /*(double) -max;*/
  long int maxindex = max * nb_steps;
  long int counter = 0;

  char format[50]; /* 50 is enough */
  int field_int_len = ( (int) log10(max)+1 ) + ( (int) log10(nb_steps) );
  /* fprintf("field_int %li %li\n",
		  (int) log10(max)+1, (int) log10(nb_steps) ); */
  sprintf(format, "  /* (%%.%ili) Phi(%% -%i.%ig) = */\t%%.%ig,",
		  (int) log10(2*maxindex - 1) +1,
		  field_int_len+2,field_int_len,DBL_DIG+2);

  printf("#ifndef _PHI_TABLE_H\n");
  printf("#define _PHI_TABLE_H\n\n");
  printf("#define CDF_MAX_INDEX %i\n", maxindex - 1);
  printf("#define CDF_X_TO_INDEX_COEFF %i\n", nb_steps);
  printf("const double CUMULATIVE_DISTRIBUTION_FUNCTION[%li] = \n",
		 maxindex);
  printf("  {\n");

  /*x = ((double)-max) + ((double)counter) / nb_steps;*/
  x = ((double)counter) / nb_steps;
  while ( x < max )
	{
	  /*printf(format, phi_std(x));*/
	  printf(format, counter, x, phi_std(x));
	  if ( counter % 1 == 0 )
		printf("\n");

	  counter++;
	  /*x = ((double)-max) + ((double)counter) / nb_steps;*/
	  x = ((double)counter) / nb_steps;
	}
  printf("  };\n\n#endif\n");
}

int main(int argc, char** argv)
{
  int max;
  int nb_steps;

  if ( argc != 3 ) {
	fprintf(stderr, "Usage: %s max nb_steps\n",argv[0]);
	return -1;
  }
  max=atoi(argv[1]);
  nb_steps = (int) ( pow(10, atoi(argv[2])) );
  fprintf(stderr,"max=%li and nb_steps=%li\n",max, nb_steps);
  gen_Phi_table(max, nb_steps);
  return 0;
}
