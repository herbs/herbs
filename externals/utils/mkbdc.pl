#!/usr/bin/perl -w

###############################################################################
# mkbdc.pl : programme pour passer de donn�es brutes � des requ�tes ins�rant  #
#    cas donn�es dans une base SQL.                                           #
#                                                                             #
# ex.: 1,1,1,0        insert into t (att1, att2, att3, att4) values (1,1,1,0) #
#      0,1,0,0   ==>  insert into t (att1, att2, att3, att4) values (0,1,0,0) #
#      1,1,0,1        insert into t (att1, att2, att3, att4) values (1,1,0,1) #
#                                                                             #
# Copyright (C) 2003 : Beno�t Vaillant, benoit.vaillant@enst-bretagne.fr      #
#                                                                             #
# This program is free software; you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation; either version 2 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Library General Public License for more details.                        #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program; if not, write to the Free Software                 #
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  #
###############################################################################


use strict;

#########################
# Bases de cas pr�tes : #
#########################

my @bdc = ("abalone",
#	   "adult",
	   "autompg",
	   "balancescale",
	   "breastcancerwisconsin",
	   "car",
	   "cmc",
#	   "covtype",
	   "cpuperformance",
	   "ecoli",
	   "flags",
	   "housing",
	   "letterrecognition",
#	   "spambase",
	   "solarflare1",
	   "solarflare2");

#############
# Variables #
#############

my ($i, $j, $k, $in, $out, $app, @att, @val);

############
# mkbdc.pl #
############

foreach $i ( @bdc ) {
    print "G�n�ration de la base de cas sql ".$i."\n";
    
    $in = "BDC-ok/".$i.".in";
    $out = "BDC-ok/".$i.".sql";

    if (! -f $in) { next; }

    open(DATA, "<$in") || die "impossible de lire le fichier $in\n";
    open(SQL, ">$out") || die "impossible d'�crire dans le fichier $out\n";
    select(SQL);
    print "use HERBS;\n";
    while(<DATA>) {
	($j = $_) =~ s/\n//g; # on vire la fin des lignes
	@att = ();
	@val = ();
	while ($j ne "") {
	    ($k = $j) =~ s/=.+//;
	    push @att, $k;
	    $j =~ s/^\w+=//;
	    ($k = $j) =~ s/^(.+?)(\w+=.+)*$/$1/;
	    $k =~ s/ $//;
	    push @val, $k;
	    if (($k = $j) =~ m/=/) {
		$j =~ s/^(.+?)\b?(\w+=.+)*$/$2/;
	    }
	    else {$j="";}
	}
	print "insert into bdc_$i (";
	print join(",",@att);
	print ") values (";
	print join(",",@val);
	print ");\n";
    }
    close(DATA);
    close(SQL);
    select(STDOUT);
}
